package com.intesens.kinto_http;

import android.util.JsonReader;

import com.android.volley.Request;
import com.android.volley.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by amalle on 16/09/16.
 */
public class Collection {

    private KintoClient kintoClient;

    private Bucket bucket;

    private String id;

    /**
     * @param client the {@link KintoClient} used for subsequent requests
     * @param bucket the bucket parent of the collection
     * @param id the id of the collection
     */
    public Collection(KintoClient client, Bucket bucket, String id) {
        this.kintoClient = client;
        this.bucket = bucket;
        this.id = id;
    }

    /**
     * @return the current collection id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the current collection parent bucket
     */
    public Bucket getBucket() {
        return bucket;
    }

    /**
     * Retrieve the collection data.
     * @return the raw data as a {@link JSONObject}
     * @throws KintoException in case of error response from Kinto
     * @throws ClientException in case of transport error
     */
    public JSONObject getData() throws KintoException, ClientException {
        KintoRequest request = kintoClient.request(ENDPOINTS.COLLECTION)
                .routeParam("bucket", bucket.getId())
                .routeParam("collection", id);
        return kintoClient.execute(request);
    }

    public JSONObject createRecord(JSONObject data) throws KintoException, ClientException, JSONException {
        JSONObject obj = new JSONObject();
        obj.put("data", data);

        KintoRequest request = kintoClient.request("POST", ENDPOINTS.RECORDS, obj)
                .routeParam("bucket", bucket.getId())
                .routeParam("collection", id);
        return kintoClient.execute(request);
    }

    public JsonReader createRecordStream(JSONObject data) throws KintoException, ClientException, JSONException {
        JSONObject obj = new JSONObject();
        obj.put("data", data);

        KintoRequest request = kintoClient.request("POST", ENDPOINTS.RECORDS, obj)
                .routeParam("bucket", bucket.getId())
                .routeParam("collection", id);
        return kintoClient.executeStream(request);
    }

    private KintoRequest getListRecordsRequest() {
        return kintoClient.request(ENDPOINTS.RECORDS)
                .routeParam("bucket", bucket.getId())
                .routeParam("collection", id);
    }

    private KintoRequest getListRecordsRequestSince(long time) {
        return kintoClient.request(ENDPOINTS.RECORDS_SINCE)
                .routeParam("bucket", bucket.getId())
                .routeParam("collection", id)
                .routeParam("since", time+"");
    }

    private KintoRequest getFindRecordsRequest(String query) {
        return kintoClient.request(ENDPOINTS.RECORDS_FIND)
                .routeParam("bucket", bucket.getId())
                .routeParam("collection", id)
                .routeParam("search", query);
    }

    /**
     * Retrieve the records list of the current collection
     * @return the records as a set of {@link JSONObject}
     * @throws KintoException in case of error response from Kinto
     * @throws ClientException in case of transport error
     */
    public List<JSONObject> listRecords() throws KintoException, ClientException, JSONException {
        List<JSONObject> records = new ArrayList<>();
        JSONObject response = kintoClient.execute(getListRecordsRequest());
        JSONArray data = response.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            records.add(data.getJSONObject(i));
        }
        return records;
    }

    public List<JSONObject> listRecordsSince(long time) throws KintoException, ClientException, JSONException {
        List<JSONObject> records = new ArrayList<>();
        JSONObject response = kintoClient.execute(getListRecordsRequestSince(time));
        JSONArray data = response.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            records.add(data.getJSONObject(i));
        }
        return records;
    }

    public List<JSONObject> findRecords(String query) throws KintoException, ClientException, JSONException {
        List<JSONObject> records = new ArrayList<>();
        JSONObject response = kintoClient.execute(getFindRecordsRequest(query));
        JSONArray data = response.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            records.add(data.getJSONObject(i));
        }
        return records;
    }

    public JsonReader findRecordsStream(String query) throws IOException, KintoException, ClientException {
        return kintoClient.executeStream(getFindRecordsRequest(query));
    }

    /**
     * Retrieve a record from the current collection
     * @param id the record id to retrieve
     * @return the record as a raw JSONObject
     * @throws KintoException in case of error response from Kinto
     * @throws ClientException in case of transport error
     */
    public JSONObject getRecord(String id) throws KintoException, ClientException {
        KintoRequest request = kintoClient.request(ENDPOINTS.RECORD)
                .routeParam("bucket", bucket.getId())
                .routeParam("collection", this.id)
                .routeParam("record", id);
        return kintoClient.execute(request);
    }
}

package com.intesens.kinto_http;


import com.intesens.kinto_http.utils.Base64Coder;
import com.intesens.kinto_http.utils.URLParamEncoder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;


/**
 * Created by kbraham on 1/3/17.
 */

public class KintoRequest {

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private String method;
    private String url;
    private JSONObject body;
    Map<String, String> params;

    public KintoRequest(String method, String url, JSONObject body) {
        this.method = method;
        this.url = url;
        this.body = body;

        params = new HashMap<>();
        params.put("Accept", "application/json");
        params.put("Content-Type", "application/json");
    }



    public KintoRequest header(String name, String value){
        params.put(name, value);
        return this;
    }

    public KintoRequest headers(Map<String, String> headers){
        params.putAll(headers);
        return this;
    }

    public KintoRequest basicAuth(String username, String password) {
        header("Authorization", "Basic " + Base64Coder.encodeString(username + ":" + password));
        return this;
    }

    public KintoRequest routeParam(String name, String value) {
        Matcher matcher = Pattern.compile("\\{" + name + "\\}").matcher(url);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        if (count == 0) {
            throw new RuntimeException("Can't find route parameter name \"" + name + "\"");
        }
        this.url = url.replaceAll("\\{" + name + "\\}", value) ;
        return this;
    }


    public Request getRequest() {
        RequestBody body = null;
        if(this.body != null){
            body = RequestBody.create(JSON, this.body.toString());
        }

        Request.Builder requestB = new Request.Builder()
                .url(url)
                .method(method, body);

        for (Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            requestB.header(key, value);
        }

        return requestB.build();
    }

    public String getUrl() {
        return url;
    }
}

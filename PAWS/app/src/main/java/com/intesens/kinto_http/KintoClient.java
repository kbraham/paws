package com.intesens.kinto_http;

import android.content.Context;
import android.util.JsonReader;
import android.util.Log;

import com.intesens.kinto_http.utils.Base64Coder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by amalle on 16/09/16.
 */
public class KintoClient {

    private final OkHttpClient client = new OkHttpClient();

    /**
     * The remote server base URL.
     */
    private String remote;

    /**
     * Custom headers added to each requests
     */
    private Map<String, String> headers;

    private String username, password;

    /**
     * Simple constructor that allows to talk to a kinto store. Using this constructor does not allow automatic
     * response deserialization of custom types.
     *
     * @param remote The remote URL
     *               Must contain the version
     * @throws IllegalArgumentException if remote is null or an empty String
     */
    public KintoClient(String remote) {
        setRemote(remote);
    }

    /**
     * Simple constructor that allows to talk to a kinto store and also allows to configure default headers. Using
     * this constructor does not allow automatic response deserialization of custom types.
     *
     * @param remote  The remote URL
     *                Must contain the version
     * @param headers Custom http headers added to each requests
     * @throws IllegalArgumentException if remote is null or an empty String or if headers is null
     */
    public KintoClient(String remote, Map<String, String> headers) {
        this(remote);
        setHeaders(headers);
    }

    /**
     * setter for remote
     *
     * @param remote The remote URL. Must contain the version
     * @throws IllegalArgumentException if remote is null or an empty String
     */
    public void setRemote(String remote) {
        if (remote == null || remote.length() <= 0) {
            throw new IllegalArgumentException("Illegal remote argument : \"" + remote + "\"");
        }
        // TODO check version
        this.remote = remote;
    }

    /**
     * getter for remote
     *
     * @return the current remote
     */
    public String getRemote() {
        return remote;
    }

    /**
     * Custom http headers that will be added to each request
     *
     * @param headers a map of http headers fields and values
     * @throws IllegalArgumentException if headers is null
     */
    public void setHeaders(Map<String, String> headers) {
        if (headers == null) {
            throw new IllegalArgumentException("Illegal headers argument : \"" + headers + "\"");
        }
        this.headers = headers;
    }

    public Map<String, String> getHeaders() {
        return Collections.unmodifiableMap(headers);
    }

    /**
     * @return true if default header are provided
     */
    public boolean isHeaders() {
        return headers != null && !headers.isEmpty();
    }

    /**
     * Retrieves the list of buckets
     *
     * @return a list of buckets
     * @throws KintoException  in case of error response from Kinto
     * @throws ClientException in case of transport error
     */
    public JSONObject listBuckets() throws ClientException, KintoException {
        return execute(request(ENDPOINTS.BUCKETS));
    }

    /**
     * Retrieve a bucket objet to perform operations on it
     *
     * @param name the bucket name
     * @return a {@link Bucket}
     */
    public Bucket bucket(String name) {
        return new Bucket(this, name);
    }

    KintoRequest request(ENDPOINTS endpoint){
        return request("GET", endpoint, null);
    }

    KintoRequest request(String method, ENDPOINTS endpoint){
        return request(method, endpoint, null);
    }

    /**
     * Prepare a get request for the requested endpoint
     * Add default http headers (Accept, Content-Type)
     * Then add custom headers {@link #setHeaders(Map)}
     *
     * @param method HTTP method for request
     * @param endpoint the {@link ENDPOINTS} to request to
     * @return a {@link KintoRequest} object
     */
    KintoRequest request(String method, ENDPOINTS endpoint, JSONObject body) {


        String url = remote + endpoint.getPath();
        KintoRequest kr = new KintoRequest(method, url, body);



        if (isHeaders()) {
            kr.headers(headers);
        }

        if(username != null){
            kr.basicAuth(username, password);
        }

        return kr;
    }

    /**
     * Execute the given request
     *
     * @param request the {@link KintoRequest} object to execute
     * @return the response body as a JSONObject
     * @throws KintoException  if there is a kinto error
     * @throws ClientException if there is a http transport error
     */
    JSONObject execute(KintoRequest request) throws KintoException, ClientException {

            try {
                Request r = request.getRequest();
                Log.i("KINTO", "Building request " + r.toString() + " (" + r.method() + ") ");
                Response response = client.newCall(request.getRequest()).execute();
                if (!response.isSuccessful()) throw new KintoException("Unexpected code " + response.code());

//        Headers responseHeaders = response.headers();
//        for (int i = 0; i < responseHeaders.size(); i++) {
//            System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
//        }

                String body = response.body().string();
                response.body().close();

                JSONObject res = new JSONObject(body);
                return res;
            } catch (JSONException e) {
                throw new ClientException("Error during \"" + request.getUrl() + "\"", e);
            } catch (IOException e) {
                throw new ClientException("Error during \"" + request.getUrl() + "\"", e);
            }
    }

    JsonReader executeStream(KintoRequest request) throws KintoException, ClientException {

        try {
            Request r = request.getRequest();
            Log.i("KINTO", "Building request " + r.toString() + " (" + r.method() + ") ");
            Response response = client.newCall(request.getRequest()).execute();
            if (!response.isSuccessful()) throw new KintoException("Unexpected code " + response.code());

//        Headers responseHeaders = response.headers();
//        for (int i = 0; i < responseHeaders.size(); i++) {
//            System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
//        }

            Reader reader = response.body().charStream();
            response.body().close();

            return new JsonReader(reader);

        } catch (IOException e) {
            throw new ClientException("Error during \"" + request.getUrl() + "\"", e);
        }
    }

    public void setAuth(String name, String pass){
        this.username = name;
        this.password = pass;
    }
}

package eu.braham.apps.paws.processor;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import eu.braham.apps.paws.services.UbloxOfflineService;

/**
 * Created by kbraham on 11/18/16.
 */

public class AGPSProcessor {

    private ArrayList<byte[]> mAGPSBuffer = new ArrayList<>();
    private int mAGPSIndex = 0;

    public void parseAGPS(File ubloxAgps) throws IOException {

        //char []ublox_agps = new char[1024];
        byte []ublox = null;// = new char[84];
        byte check_sum_a = 0, check_sum_b = 0;
        int len = 0, pkt_start = 0, i = 0, pkt_len = 0, j = 0, pkt_cnt = 0;
        String log;
        int size = (int)ubloxAgps.length();
        byte[] ublox_agps = new byte[size];
        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(ubloxAgps));
        buf.read(ublox_agps, 0, ublox_agps.length);
        buf.close();

        i = 0;
        while(i < size) {
            if ((i > 0) && ((ublox_agps[i] & 0xFF) == 0x62) && ((ublox_agps[i - 1] & 0xFF) == 0xB5)) {
                if(pkt_start == 0) {
                    pkt_start = 1;
                    j = i - 1;
                } else {
                    ublox = new byte[i - 1 - j];
                    System.arraycopy(ublox_agps, j, ublox, 0, i - 1 - j);
                    mAGPSBuffer.add(ublox);
                    j = i -1;

                    if((i - 1 - j) > pkt_len) {
                        pkt_len = i - 1 - j;
                    }
                }
            }
            i++;
        }
        if(j < size) {
            ublox = new byte[size - j];
            System.arraycopy(ublox_agps, j, ublox, 0, i - j);
            mAGPSBuffer.add(ublox);
            if((i - j) > pkt_len) {
                pkt_len = i - j;
            }
        }

    }

    public byte[] getCurrent(){
        byte[] b;
        if(mAGPSIndex < mAGPSBuffer.size() - 1) {
            b = mAGPSBuffer.get(mAGPSIndex);
        } else {
            b = null;
        }

        return b;
    }

    public byte[] getNext(){
        mAGPSIndex++;
        return getCurrent();
    }

    public int getSize(){
        return mAGPSBuffer.size();
    }

    public int getIndex(){
        return mAGPSIndex;
    }

    public boolean isReady(){
        return mAGPSBuffer.size() > 0;
    }

}

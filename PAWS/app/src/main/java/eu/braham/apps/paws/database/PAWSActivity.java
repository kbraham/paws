package eu.braham.apps.paws.database;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by kbraham on 12/7/16.
 */

public class PAWSActivity {

    private long _id;
    private int type;
    private long time;
    private float distance;

    private ArrayList<LatLng> points;

    public PAWSActivity(long id, int type, long time) {
        this._id = id;
        this.type = type;
        this.time = time;
        this.distance = 0;
    }

    public long getId() {
        return _id;
    }

    public int getType() {
        return type;
    }

    public long getTime() {
        return time;
    }

    public void addDistance(float dist){
        this.distance += dist;
    }

    public float getDistance() {
        return distance;
    }

    public void releasePoints() {
        this.points = null;
    }

    public void addPoint(LatLng p) {
        if (this.points == null) {
            this.points = new ArrayList<>();
        }

        this.points.add(p);
    }

    public void addPoint(int pos, LatLng p) {
        if (this.points != null) {
            this.points.add(pos, p);
        }
    }

    public ArrayList<LatLng> getPoints() {
        return points;
    }

    public int getPointsCount(){
        if(this.points == null){
            return 0;
        }
        return this.points.size();
    }

    public LatLng getLastPoint(){
        if(this.points == null){
            return null;
        }
        return this.points.get(this.points.size()-1);
    }
}

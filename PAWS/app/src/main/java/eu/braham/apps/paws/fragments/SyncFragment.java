package eu.braham.apps.paws.fragments;


import android.database.sqlite.SQLiteStatement;
import android.support.v4.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

import eu.braham.apps.paws.R;
import eu.braham.apps.paws.database.PAWSDBHelper;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SyncFragment extends Fragment {

    public static final String TAG = "SyncFragment";


    private TextView mTVActivity;
    private ImageView mIVActivity;
    private PAWSDBHelper mDbHelper;

    public SyncFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDbHelper = new PAWSDBHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sync, container, false);

        mTVActivity = (TextView) v.findViewById(R.id.tvActivity);
        mIVActivity = (ImageView) v.findViewById(R.id.ivActivity);
        Button b = (Button) v.findViewById(R.id.btnUpload);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    doUpload();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        Button btnExport = (Button) v.findViewById(R.id.btnExport);
        btnExport.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {


                                             File db = getActivity().getDatabasePath(PAWSDBHelper.DATABASE_NAME);
                                             Log.e(TAG, "PATH: " + db.getAbsolutePath());
                                             if (db.exists()) {
                                                 try {
                                                     copyFile(new FileInputStream(db), new FileOutputStream("/sdcard/paws.db"));
                                                 } catch (FileNotFoundException e) {
                                                     e.printStackTrace();
                                                 } catch (IOException e) {
                                                     e.printStackTrace();
                                                 }
                                                 Log.e(TAG, "Copied db!");
                                             } else {
                                                 Log.e(TAG, "unable to find db!");
                                             }
                                         }
                                     }
        );


        Button btnImport = (Button) v.findViewById(R.id.btnImport);
        btnImport.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {


                                             File db = new File("/sdcard/paws.db");
                                             File dest = getActivity().getDatabasePath(PAWSDBHelper.DATABASE_NAME);
                                             Log.e(TAG, "PATH: " + db.getAbsolutePath() + " DEST: " + dest.getAbsolutePath());
                                             if (db.exists()) {
                                                 try {
                                                     copyFile(new FileInputStream(db), new FileOutputStream(dest));
                                                     mDbHelper.getWritableDatabase().close();
                                                     Log.e(TAG, "Copied db!");
                                                     loadLastActivity();
                                                 } catch (FileNotFoundException e) {
                                                     e.printStackTrace();
                                                 } catch (IOException e) {
                                                     e.printStackTrace();
                                                 }

                                             } else {
                                                 Log.e(TAG, "unable to find db!");
                                             }
                                         }
                                     }
        );

loadLastActivity();

        return v;
    }

    private void loadLastActivity(){

        // Load activity data

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                PAWSDBHelper.ActivityEntry._ID,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_NAME,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME
        };


        Cursor c = db.query(
                PAWSDBHelper.ActivityEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                "_id DESC",
                "1"
        );

        c.moveToFirst();
        if (c.getCount() > 0) {

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            long aid = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry._ID));
            long time = c.getLong(
                    c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME)
            );
            int type = c.getInt(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE));
            String name = c.getString(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_NAME));

            long timeSpent = getActivityDuration(aid);

            int hours = (int) timeSpent / 3600;
            int remainder = (int) timeSpent - hours * 3600;
            int mins = remainder / 60;
            remainder = remainder - mins * 60;
            int secs = remainder;


            String timeformatted = "";

            if(hours > 0){
                timeformatted += hours + ":";
            }
            if(mins > 0 || hours > 0){
                timeformatted += mins + ":";
            }
            timeformatted += secs;

            mTVActivity.setText(name + "\n" + dateFormat.format(new Date(time * 1000)) + "\n Duration: " + timeformatted);

            // todo: check functionality
            switch (type) {
                case 1:
                    mIVActivity.setImageDrawable(getResources().getDrawable(R.mipmap.hond1));
                    break;
                case 2:
                    mIVActivity.setImageDrawable(getResources().getDrawable(R.mipmap.hond1));
                    break;
                case 3:
                    mIVActivity.setImageDrawable(getResources().getDrawable(R.mipmap.hond1));
                    break;
            }

        }

        db.close();
    }


    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    void post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();


        client.newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(final Call call, IOException e) {
                        // Error

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // For the example, you can show an error dialog or a toast
                                // on the main UI thread
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        final String res = response.body().string();


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // For the example, you can show an error dialog or a toast
                                // on the main UI thread
                                mTVActivity.setText("Upload succesful!\n" + res);
                            }
                        });
                        // Do something with the response

                    }
                });
    }

    String bowlingJson(String player1, String player2) {
        return "{'winCondition':'HIGH_SCORE',"
                + "'name':'Bowling',"
                + "'round':4,"
                + "'lastSaved':1367702411696,"
                + "'dateStarted':1367702378785,"
                + "'players':["
                + "{'name':'" + player1 + "','history':[10,8,6,7,8],'color':-13388315,'total':39},"
                + "{'name':'" + player2 + "','history':[6,10,5,10,10],'color':-48060,'total':41}"
                + "]}";
    }


    private Cursor getAcceleroForAID(long aid, SQLiteDatabase db) {
        // Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                PAWSDBHelper.AcceleroEntry._ID,
                PAWSDBHelper.AcceleroEntry.COLUMN_NAME_X,
                PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Y,
                PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Z,
                PAWSDBHelper.AcceleroEntry.COLUMN_NAME_TIMESTAMP
        };

        String whereClause = PAWSDBHelper.AcceleroEntry.COLUMN_NAME_AID + "=?";
        String[] whereArgs = {aid + ""};

        Cursor c = db.query(
                PAWSDBHelper.AcceleroEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                "_id ASC"
        );


        return c;

    }

    String convLastToJSON() {
        // Load activity data

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                PAWSDBHelper.ActivityEntry._ID,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_NAME,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME
        };


        Cursor c = db.query(
                PAWSDBHelper.ActivityEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                "_id DESC",
                "1"
        );

        c.moveToFirst();
        if (c.getCount() > 0) {
            long time = c.getLong(
                    c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME)
            );
            int type = c.getInt(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE));
            String name = c.getString(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_NAME));
            long aid = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry._ID));

            Log.e("AID", aid + "");

            StringBuilder sb = new StringBuilder();


            //
            //
            // {"name": "aw", "type": 1, "time": 230023, "accel": []}

            sb.append("{\"name\": \"").append(name).append("\", \"type\": ").append(type).append(", \"time\": ").append(time).append(", \"accel\": ");


            // Get all accelero points


            Cursor cursor = getAcceleroForAID(aid, db);


            cursor.moveToFirst();
            if (cursor.getCount() > 0) {


                sb.append("[");

                float x, y, z;
                long timestamp;
                x = cursor.getFloat(cursor.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_X));
                y = cursor.getFloat(cursor.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Y));
                z = cursor.getFloat(cursor.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Z));
                timestamp = cursor.getLong(cursor.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_TIMESTAMP));

                sb.append("{\"x\": ").append(x).append(",\"y\": ").append(y).append(",\"z\": ").append(z).append(",\"timestamp\": ").append(timestamp).append("}");

                do {
                    x = cursor.getFloat(cursor.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_X));
                    y = cursor.getFloat(cursor.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Y));
                    z = cursor.getFloat(cursor.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Z));
                    timestamp = cursor.getLong(cursor.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_TIMESTAMP));

                    sb.append(",{\"x\": ").append(x).append(",\"y\": ").append(y).append(",\"z\": ").append(z).append(",\"timestamp\": ").append(timestamp).append("}");
                } while (cursor.moveToNext());

                sb.append("]");


            }

            sb.append("}");

            db.close();
            return sb.toString();
        }


        db.close();
        return null;
    }

    private long getActivityDuration(long aid){


        long count;

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        SQLiteStatement s = db.compileStatement( "select count(*) from "+PAWSDBHelper.AcceleroEntry.TABLE_NAME+" where "+PAWSDBHelper.AcceleroEntry.COLUMN_NAME_AID+"='" + aid + "'; " );
        count = s.simpleQueryForLong();

        db.close();

        return count;
    }

    private void doUpload() throws IOException {
        //String json = bowlingJson("Jesse", "Jake");
        String json = convLastToJSON();
        post("http://paws.kb7.nl/store.php", json);
    }


    public static void copyFile(FileInputStream fromFile, FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }

}

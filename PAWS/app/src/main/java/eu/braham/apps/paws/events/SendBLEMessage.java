package eu.braham.apps.paws.events;

/**
 * Created by kbraham on 1/23/17.
 */

public class SendBLEMessage {

    private String message;

    public SendBLEMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

package eu.braham.apps.paws.processor;

import eu.braham.apps.paws.events.AcceleroEvent;

/**
 * Created by kbraham on 11/18/16.
 */

public class CircularQueue<T> {

    private AcceleroEvent[] elements;

    public CircularQueue(int size) {
        this.elements = new AcceleroEvent[size];
    }

    public void insert(AcceleroEvent ai) {
        // Shift all one position
        for (int i = 0; i < elements.length - 1; ++i) {
            elements[i] = elements[i + 1];
        }

        elements[elements.length - 1] = ai;
    }

    public AcceleroEvent[] getElements() {
        return elements;
    }

    public CircularQueue<T> clone(){
        CircularQueue<T> tmp = new CircularQueue<>(elements.length);
        System.arraycopy(elements, 0, tmp.elements, 0, elements.length);
        return tmp;
    }
}

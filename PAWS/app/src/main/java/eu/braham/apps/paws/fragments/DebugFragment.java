package eu.braham.apps.paws.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import eu.braham.apps.paws.R;
import eu.braham.apps.paws.events.BLEEvent;
import eu.braham.apps.paws.events.GPSEvent;
import eu.braham.apps.paws.events.SendBLEMessage;

/**
 * A simple {@link Fragment} subclass.
 */
public class DebugFragment extends Fragment {


    private StringBuilder mStringBuilder = new StringBuilder();
    private TextView mTxtDebug = null;
    private ProgressBar mProgress = null;

    public DebugFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View main = inflater.inflate(R.layout.fragment_debug, container, false);

        mTxtDebug = (TextView) main.findViewById(R.id.txtDebug);
        mProgress = (ProgressBar) main.findViewById(R.id.progressBar);

        Button b = (Button) main.findViewById(R.id.btnReboot);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new SendBLEMessage("reboot"));
            }
        });

        return main;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(BLEEvent event){
        mStringBuilder.insert(0, event.getMessage() + "\n");
        if(mTxtDebug != null){
            mTxtDebug.setText(mStringBuilder.toString());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(GPSEvent event){
        if(event.getStatus() == GPSEvent.STATUS_UPLOAD && mProgress != null){
            mProgress.setProgress(event.getProgress());
            mProgress.setMax(event.getProgressMax());
        }
    }
}

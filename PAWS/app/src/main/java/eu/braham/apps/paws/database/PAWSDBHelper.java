package eu.braham.apps.paws.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;


public class PAWSDBHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ACTIVITY =
            " CREATE TABLE " + ActivityEntry.TABLE_NAME + " (" +
                    ActivityEntry._ID + " INTEGER PRIMARY KEY," +
                    ActivityEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ActivityEntry.COLUMN_NAME_TYPE + INT_TYPE + COMMA_SEP +
                    ActivityEntry.COLUMN_NAME_TIME + INT_TYPE + COMMA_SEP +
                    ActivityEntry.COLUMN_NAME_DURATION + INT_TYPE + COMMA_SEP +
                    ActivityEntry.COLUMN_NAME_KINTO_ID + TEXT_TYPE + COMMA_SEP +
                    ActivityEntry.COLUMN_NAME_KINTO_LAST_MODIFIED + TEXT_TYPE +
                    " );";

    private static final String SQL_CREATE_ACCELERO =
            " CREATE TABLE " + AcceleroEntry.TABLE_NAME + " (" +
                    AcceleroEntry._ID + " INTEGER PRIMARY KEY," +
                    AcceleroEntry.COLUMN_NAME_AID + INT_TYPE + COMMA_SEP +
                    AcceleroEntry.COLUMN_NAME_X + REAL_TYPE + COMMA_SEP +
                    AcceleroEntry.COLUMN_NAME_Y + REAL_TYPE + COMMA_SEP +
                    AcceleroEntry.COLUMN_NAME_Z + REAL_TYPE + COMMA_SEP +
                    AcceleroEntry.COLUMN_NAME_TIMESTAMP + INT_TYPE + " );";

    private static final String SQL_CREATE_GPS =
            " CREATE TABLE " + GPSEntry.TABLE_NAME + " (" +
                    GPSEntry._ID + " INTEGER PRIMARY KEY," +
                    GPSEntry.COLUMN_NAME_AID + INT_TYPE + COMMA_SEP +
                    GPSEntry.COLUMN_NAME_LAT + REAL_TYPE + COMMA_SEP +
                    GPSEntry.COLUMN_NAME_LONG + REAL_TYPE + COMMA_SEP +
                    GPSEntry.COLUMN_NAME_TIMESTAMP + INT_TYPE + " );";

    private static final String SQL_CREATE_INDEX_ACCELERO =
            " CREATE INDEX " + ActivityEntry.TABLE_NAME + "_" + AcceleroEntry.TABLE_NAME + " ON " + AcceleroEntry.TABLE_NAME + "(" + AcceleroEntry.COLUMN_NAME_AID + ")";

    private static final String SQL_CREATE_INDEX_GPS =
            " CREATE INDEX " + ActivityEntry.TABLE_NAME + "_" + GPSEntry.TABLE_NAME + " ON " + GPSEntry.TABLE_NAME + "(" + GPSEntry.COLUMN_NAME_AID + ")";

    private static final String SQL_DELETE_ACTIVITY =
            " DROP TABLE IF EXISTS " + ActivityEntry.TABLE_NAME + ";";

    private static final String SQL_DELETE_ACCELERO = " DROP TABLE IF EXISTS " + AcceleroEntry.TABLE_NAME + ";";
    private static final String SQL_DELETE_GPS = " DROP TABLE IF EXISTS " + GPSEntry.TABLE_NAME + ";";


    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 7;
    public static final String DATABASE_NAME = "PAWS.db";

    public PAWSDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ACTIVITY);
        db.execSQL(SQL_CREATE_ACCELERO);
        db.execSQL(SQL_CREATE_GPS);
        db.execSQL(SQL_CREATE_INDEX_ACCELERO);
        db.execSQL(SQL_CREATE_INDEX_GPS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over

        if(oldVersion == 6 && newVersion == 7){
            Log.w("PAWSDBHelper", "Upgrading DB by adding kotin activity");
            db.execSQL("ALTER TABLE "+ActivityEntry.TABLE_NAME+" ADD COLUMN "+ActivityEntry.COLUMN_NAME_KINTO_ID +TEXT_TYPE+";");
            db.execSQL("ALTER TABLE "+ActivityEntry.TABLE_NAME+" ADD COLUMN "+ActivityEntry.COLUMN_NAME_KINTO_LAST_MODIFIED +TEXT_TYPE+";");
        } else if(oldVersion == 5 && newVersion == 6){
            Log.w("PAWSDBHelper", "Upgrading DB by adding indexes");
            db.execSQL(SQL_CREATE_INDEX_ACCELERO);
            db.execSQL(SQL_CREATE_INDEX_GPS);
        } else {

            Log.w("PAWSDBHelper", "Removing database!" + oldVersion + " " + newVersion);
            db.execSQL(SQL_DELETE_ACTIVITY);
            db.execSQL(SQL_DELETE_ACCELERO);
            db.execSQL(SQL_DELETE_GPS);
            onCreate(db);
        }
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public static class ActivityEntry implements BaseColumns {
        public static final String TABLE_NAME = "activity";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_DURATION = "duration";
        public static final String COLUMN_NAME_KINTO_ID = "kotin_id";
        public static final String COLUMN_NAME_KINTO_LAST_MODIFIED = "klastmodified";
    }


    public static class AcceleroEntry implements BaseColumns {
        public static final String TABLE_NAME = "accelero";
        public static final String COLUMN_NAME_AID = "aid";
        public static final String COLUMN_NAME_X = "x";
        public static final String COLUMN_NAME_Y = "y";
        public static final String COLUMN_NAME_Z = "z";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
    }

    public static class GPSEntry implements BaseColumns {
        public static final String TABLE_NAME = "gps";
        public static final String COLUMN_NAME_AID = "aid";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LONG = "long";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
    }
}
package eu.braham.apps.paws.events;

/**
 * Created by coen_ on 14/11/2016.
 */

public class ActivityEvent {

    private int activity;

    public ActivityEvent (int activity) {
        this.activity = activity;
    }

    public int getActivity() {
        return activity;
    }
}

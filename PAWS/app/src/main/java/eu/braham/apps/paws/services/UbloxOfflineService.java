package eu.braham.apps.paws.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;

import eu.braham.apps.paws.PrivateTokens;

public class UbloxOfflineService extends IntentService {
    public static final String NOTIFICATION = "net.iot_platform.oser.ubloxofflineservice";
    public static final String FILE_NAME = "online1.ubx";
    public static final String TAG = "UBLOX";

    public UbloxOfflineService() {
        super("ublox offline download service");
    }

    private void downloadUbloxData() {
        try {
            String ublox = "http://online-live1.services.u-blox.com/GetOnlineData.ashx?token="+ PrivateTokens.UBLOX_TOKEN+";gnss=gps;datatype=eph,alm,aux";
            URL url = new URL(ublox);
            URLConnection connection = url.openConnection();
            connection.connect();

            int fileLength = connection.getContentLength();

            File sdcard = Environment.getExternalStorageDirectory();
            //File ubloxAgps = new File(sdcard, "online1.ubx");
            File ubloxAgps = new File(getFilesDir(), FILE_NAME);

            InputStream input = new BufferedInputStream(connection.getInputStream());
            OutputStream output = new FileOutputStream(ubloxAgps);


            byte data[] = new byte[1024];
            long total = 0;
            int count;

            while((count = input.read(data)) != -1) {
                total += count;

                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();

            Intent intent = new Intent(NOTIFICATION);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        } catch (Exception ex) {
            Log.i(TAG, ex.toString());
        }
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        File sdcard = Environment.getExternalStorageDirectory();

        File ubloxAgps = new File(getFilesDir(), FILE_NAME);

        Log.i(TAG, ubloxAgps.toString());

        if(ubloxAgps.exists()) {
            Date lastModDate = new Date(ubloxAgps.lastModified());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(lastModDate);
            calendar.add(Calendar.HOUR, 2);

            Calendar now = Calendar.getInstance();
            now.setTime(new Date());
            //agps data needs to be updated
            if(calendar.compareTo(now) < 0) {
                Log.i(TAG, "need to download new agps data\n");
                downloadUbloxData();
            } else {
                Log.i(TAG, "agps data is still valid\n");
                Intent intentNotification = new Intent(NOTIFICATION);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intentNotification);
            }
        } else {
            Log.i(TAG, "need to download new agps data\n");
            downloadUbloxData();
        }
    }
}
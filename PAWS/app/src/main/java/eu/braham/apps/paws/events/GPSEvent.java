package eu.braham.apps.paws.events;

import android.util.Log;

/**
 * Created by kbraham on 10/4/16.
 *
 * examples
 *  $GNGGA,,,,,,0,00,99.
 *  $GNGGA,193855.00,,,,
 *
 *
 *
 *  10-04 22:46:23.580 24313-24313/eu.braham.apps.paws D/TagDemo: BLE 5215.08341��N��
 10-04 22:46:23.630 24313-24313/eu.braham.apps.paws D/TagDemo: BLE 00651.03845��E��
 10-04 22:46:24.605 24313-24313/eu.braham.apps.paws D/TagDemo: BLE 5215.08206��N��
 10-04 22:46:24.650 24313-24313/eu.braham.apps.paws D/TagDemo: BLE 00651.03877��E��
 */

public class GPSEvent {
    public final static int STATUS_UPLOAD = 0;
    public final static int STATUS_SEARCHING = 1;
    public final static int STATUS_TIME = 2;
    public final static int STATUS_FIX = 3;

    private static double last_lat;
    private static String last_long;

    private int status;
    private int progress;
    private int progressMax;
    private double lat;
    private double lon;

    private String timestamp;

    public GPSEvent() {
    }

    public GPSEvent(int status, int progress, int progressMax) {
        this.status = status;
        this.progress = progress;
        this.progressMax = progressMax;
    }

    public static GPSEvent parseText(String txt){
        GPSEvent gi = new GPSEvent();

        //L3,5215.01886,N,0051
        //5.21414,E

        Log.i("GPS", txt);
        String[] parts = txt.substring(3).split(",");

        if(txt.startsWith("L1")){
            // searching
            gi.status = STATUS_SEARCHING;
        } else if(txt.startsWith("L2")){
            // timestamp
            gi.status = STATUS_TIME;
        } else if(txt.startsWith("L3")){
            // N coord
            gi = null;

            double deg = Double.parseDouble(parts[0].substring(0,2));
            double mins = Double.parseDouble(parts[0].substring(2));
            deg += (mins*60)/3600;

            last_lat = deg;
            last_long = parts[2];

            // TODO: North/South addition
        } else {
            // E coord
            gi.status = STATUS_FIX;

            parts = txt.split(",");
            if(!last_long.equals("")){
                double deg = Double.parseDouble(last_long.substring(0,3));
                double mins = Double.parseDouble(last_long.substring(3) + parts[0]);
                deg += (mins*60)/3600;
                gi.lat = last_lat;
                gi.lon = deg;
            }

            // TODO: West/East addition
        }

        return gi;
    }

    public int getStatus() {
        return status;
    }

    public int getProgressMax() {
        return progressMax;
    }

    public int getProgress() {
        return progress;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getStatusString(){
        switch (status){
            case STATUS_SEARCHING:
                return "Searching";
            case STATUS_FIX:
                return "Fix";
            case STATUS_UPLOAD:
                return "Uploading";
            case STATUS_TIME:
                return "Time fixed";
        }

        return null;
    }

    public String toString(){
        return "GPSEvent (" + getStatusString() + ", " + timestamp + ")";
    }
}

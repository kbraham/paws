package eu.braham.apps.paws.utils;

/**
 * Created by kbraham on 1/20/17.
 */

public class TimeFormatter {
    public static String format(int time){
        int hours = time / 3600;
        int remainder = time - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        String timeformatted;
        if(time > 3600){
            timeformatted = (hours < 10? "0"+hours:hours) + ":" + (mins < 10? "0"+mins:mins) + "h";
        } else if (time > 60){
            timeformatted = (mins < 10? "0"+mins:mins) + ":" + (secs < 10? "0"+secs:secs) + "m";
        } else {
            timeformatted = (secs < 10? "0"+secs:secs) + "s";
        }

        return timeformatted;
    }
}

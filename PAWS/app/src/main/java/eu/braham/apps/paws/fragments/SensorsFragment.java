package eu.braham.apps.paws.fragments;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import eu.braham.apps.paws.R;
import eu.braham.apps.paws.events.AcceleroEvent;
import eu.braham.apps.paws.events.ActivityEvent;
import eu.braham.apps.paws.events.BufferRequestEvent;
import eu.braham.apps.paws.events.BufferResponseEvent;
import eu.braham.apps.paws.events.GPSEvent;
import eu.braham.apps.paws.events.GyroEvent;
import eu.braham.apps.paws.events.SendBLEMessage;
import eu.braham.apps.paws.processor.ActivityRecognizer;
import eu.braham.apps.paws.processor.CircularQueue;
import eu.braham.apps.paws.services.SensorsService;

public class SensorsFragment extends Fragment {

    private final static String TAG = "SensorsFragment";
    private CircularQueue<AcceleroEvent> queue = new CircularQueue<>(SensorsService.MAX_ACCELERO_EVENTS);

    private TextView mTextviewAccelerometerX;
    private TextView mTextviewAccelerometerY;
    private TextView mTextviewAccelerometerZ;
    private TextView mTextviewGyroscopeX;
    private TextView mTextviewGyroscopeY;
    private TextView mTextviewGyroscopeZ;
    private TextView mTextviewGPSstatus;
    private TextView mTextviewGPSlat;
    private TextView mTextviewGPSlon;
    private TextView mTextviewGPStime;
    private TextView mTextviewActivitystatus;
    private LineChart mgLCaccelerometer;



    public SensorsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sensors, container, false);
        mTextviewAccelerometerX = (TextView) v.findViewById(R.id.tvAccelerometerX);
        mTextviewAccelerometerY = (TextView) v.findViewById(R.id.tvAccelerometerY);
        mTextviewAccelerometerZ = (TextView) v.findViewById(R.id.tvAccelerometerZ);
        mTextviewGyroscopeX = (TextView) v.findViewById(R.id.tvGyroscopeX);
        mTextviewGyroscopeY = (TextView) v.findViewById(R.id.tvGyroscopeY);
        mTextviewGyroscopeZ = (TextView) v.findViewById(R.id.tvGyroscopeZ);
        mTextviewGPSstatus = (TextView) v.findViewById(R.id.tvGPSstatus);
        mTextviewGPSlat = (TextView) v.findViewById(R.id.tvGPSlat);
        mTextviewGPSlon = (TextView) v.findViewById(R.id.tvGPSlon);
        mTextviewGPStime = (TextView) v.findViewById(R.id.tvGPStime);
        mTextviewActivitystatus = (TextView) v.findViewById(R.id.tvActivitystatus);
        mgLCaccelerometer = (LineChart) v.findViewById(R.id.gLCaccelerometer);

        Button b = (Button) v.findViewById(R.id.btnReboot);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new SendBLEMessage("reboot"));
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        EventBus.getDefault().register(this);
        EventBus.getDefault().post(new BufferRequestEvent());
    }

    @Override
    public void onDetach() {
        super.onDetach();

        EventBus.getDefault().unregister(this);
    }
//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//
//        inflater.inflate(R.menu.sensors_fragment, menu);
//
//        if (!false) {
//            menu.findItem(R.id.menu_record_stop).setVisible(false);
//            menu.findItem(R.id.menu_record_start).setVisible(true);
//        } else {
//            menu.findItem(R.id.menu_record_stop).setVisible(true);
//            menu.findItem(R.id.menu_record_start).setVisible(false);
//        }
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_record_start:
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                builder.setTitle("Activity name?");
//
//                // Set up the input
//                final EditText input = new EditText(getActivity());
//                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
//                input.setInputType(InputType.TYPE_CLASS_TEXT);
//                builder.setView(input);
//
//                // Set up the buttons
//                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        addActivity(input.getText().toString(), 1, System.currentTimeMillis() / 1000, 300);
//                        mRecording = true;
//                        getActivity().invalidateOptionsMenu();
//                    }
//                });
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        mRecording = false;
//                        getActivity().invalidateOptionsMenu();
//                        dialog.cancel();
//                    }
//                });
//
//                builder.show();
//
//                Toast.makeText(getContext(), "Scan!", Toast.LENGTH_SHORT);
//                getActivity().invalidateOptionsMenu();
//                break;
//            case R.id.menu_record_stop:
//                mRecording = false;
//                Toast.makeText(getContext(), "Stop!", Toast.LENGTH_SHORT);
//                getActivity().invalidateOptionsMenu();
//                break;
//        }
//        return true;
//    }


    private void showMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(ActivityEvent event) {
        // Have we left the fragment?
        if (mTextviewAccelerometerX == null) {
            return;

        }
        int state = event.getActivity();
        if ((state == ActivityRecognizer.ACTIVITY_REST)) {
            mTextviewActivitystatus.setText("Resting");
        }
        else if (state == ActivityRecognizer.ACTIVITY_WALK) {
            mTextviewActivitystatus.setText("Walking");
        }
        else{ mTextviewActivitystatus.setText("Running");
        }

    }
        // Called in Android UI's main thread
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(AcceleroEvent event) {
        // Have we left the fragment?
        if (mTextviewAccelerometerX == null) {
            return;
        }



        mTextviewAccelerometerX.setText(event.getX() + "");
        mTextviewAccelerometerY.setText(event.getY() + "");
        mTextviewAccelerometerZ.setText(event.getZ() + "");
        queue.insert(event);


        List<Entry> entriesX = new ArrayList<Entry>();
        List<Entry> entriesY = new ArrayList<Entry>();
        List<Entry> entriesZ = new ArrayList<Entry>();

        AcceleroEvent[] buffer = queue.getElements();
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] == null) {
                continue;
            }
            entriesX.add(new Entry(i, (float) buffer[i].getX()));
            entriesY.add(new Entry(i, (float) buffer[i].getY()));
            entriesZ.add(new Entry(i, (float) buffer[i].getZ()));
        }

        LineDataSet setX = new LineDataSet(entriesX, "X-values");
        LineDataSet setY = new LineDataSet(entriesY, "Y-values");
        LineDataSet setZ = new LineDataSet(entriesZ, "Z-values");
        setX.setColor(Color.rgb(255, 0, 0));   // red
        setY.setColor(Color.rgb(0, 128, 0));   // green
        setZ.setColor(Color.rgb(255, 140, 0)); // dark orange
        setX.setDrawCircles(false);
        setY.setDrawCircles(false);
        setZ.setDrawCircles(false);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(setX);
        dataSets.add(setY);
        dataSets.add(setZ);

        LineData lineData = new LineData(dataSets);
        lineData.setDrawValues(false);
        mgLCaccelerometer.setData(lineData);
        Description d = new Description();
        d.setText("Accelerometer values");
        mgLCaccelerometer.setDescription(d);
        mgLCaccelerometer.invalidate();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(GPSEvent event){
        if (mTextviewGPSstatus == null) {
            return;
        }

        mTextviewGPSstatus.setText(event.getStatusString());
        mTextviewGPSlat.setText(event.getLat() + "");
        mTextviewGPSlon.setText(event.getLon() + "");
        mTextviewGPStime.setText(event.getTimestamp() + "");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(GyroEvent event){
        if (mTextviewGyroscopeX == null) {
            return;
        }

        mTextviewGyroscopeX.setText(event.getX() + "");
        mTextviewGyroscopeY.setText(event.getY() + "");
        mTextviewGyroscopeZ.setText(event.getZ() + "");
    }

    @Subscribe
    public void onMessage(BufferResponseEvent event){
        this.queue = event.getQueue();
    }
}

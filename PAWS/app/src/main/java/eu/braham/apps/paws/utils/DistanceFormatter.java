package eu.braham.apps.paws.utils;

/**
 * Created by kbraham on 1/20/17.
 */

public class DistanceFormatter {

    public static String format(float distanceIn){
        float distance = Math.round(distanceIn);
        String text;
        if(distance > 1000){
            distance /= 100;
            distance = Math.round(distance);
            distance /= 10;
            text = (int)distance + "km";
        } else {
            text = (int)distance + "m";
        }
        return text;
    }
}

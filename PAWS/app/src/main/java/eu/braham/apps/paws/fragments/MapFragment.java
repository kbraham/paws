package eu.braham.apps.paws.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import eu.braham.apps.paws.R;
import eu.braham.apps.paws.database.PAWSActivity;
import eu.braham.apps.paws.database.PAWSDBHelper;
import eu.braham.apps.paws.processor.ActivityRecognizer;
import eu.braham.apps.paws.processor.BackgroundRunner;
import eu.braham.apps.paws.utils.TimeFormatter;
import eu.braham.apps.paws.utils.DistanceFormatter;

/**
 * Created by kbraham on 10/13/16.
 */

public class MapFragment extends Fragment {

    private final static String TAG = "MapFragment";

    // the fragment initialization parameters
    private static final String ARG_START_TIME = "startTime";
    private static final String ARG_END_TIME = "endTime";

    // Timestamps for data fetching, will be overridden by activity arguments
    private long mStartTime = 1481109528;
    private long mEndTime = 1581109528;

    private MapView mMapView;
    private GoogleMap googleMap;
    private PAWSDBHelper mDbHelper;

    private TextView mDurationWalk;
    private TextView mDistanceWalk;
    private TextView mDurationRun;
    private TextView mDistanceRun;

    private BarChart mBarChart;
    private final float groupSpace = 0.07f;
    private final float barSpace = 0.01f;
    private final float barWidth = 0.30f;

    private final static long MAX_TIME_DIFFERENCE_FOR_CONNECTION = 300;

    private ArrayList<PAWSActivity> mActivities = null;
    private LatLngBounds.Builder mBuilder = new LatLngBounds.Builder();
    private long mActivityLengthWalk = 0;
    private float mGPSLengthWalk = 0;
    private long mActivityLengthRun = 0;
    private float mGPSLengthRun= 0;
    private int mNumTraces = 0;
    private boolean mapUpdated = false;

    private Handler mHandler = new Handler();

    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param start Starting time of the timeframe to display
     * @param end   End time of the timeframt to display
     * @return A new instance of fragment MapFragment.
     */
    public static MapFragment newInstance(long start, long end) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_START_TIME, start);
        args.putLong(ARG_END_TIME, end);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStartTime = getArguments().getLong(ARG_START_TIME);
            mEndTime = getArguments().getLong(ARG_END_TIME);
        }

        mDbHelper = new PAWSDBHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mDurationWalk = (TextView) rootView.findViewById(R.id.durationWalk);
        mDistanceWalk = (TextView) rootView.findViewById(R.id.distanceWalk);
        mDurationRun = (TextView) rootView.findViewById(R.id.durationRun);
        mDistanceRun = (TextView) rootView.findViewById(R.id.distanceRun);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mBarChart = (BarChart) rootView.findViewById(R.id.HourlyOverview);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Run expensive loading tasks in background
        BackgroundRunner.runTask(new Runnable() {
            @Override
            public void run() {

                mapUpdated = false;
                mBuilder = new LatLngBounds.Builder();

                // GET last walk

                SQLiteDatabase db = mDbHelper.getReadableDatabase();

                // Get list of todays mActivities


                long stopWS = System.currentTimeMillis();


                long stopABS = System.currentTimeMillis();
                ArrayList<PAWSActivity> activities = getActivitiesBetween(db, mStartTime, mEndTime);
                long stopABE = System.currentTimeMillis();
                Log.e("MAp FRAGMENT", "Time to load overview activities and GPS: " + (stopABE - stopABS));

                if (activities == null) {
//                    db.close();
                    return;
                }

                mActivityLengthWalk = 0;
                mGPSLengthWalk = 0;
                mActivityLengthRun = 0;
                mGPSLengthRun = 0;
                long lastTime = 0;
                int count = 0;

                HashMap<Long, PAWSActivity> acts = new HashMap<>();
                String[] actIDs = new String[]{activities.get(0).getId() + "", activities.get(activities.size() - 1).getId() + ""};


                for (PAWSActivity act : activities) {
                    acts.put(act.getId(), act);
                }

                long stopFS = System.currentTimeMillis();

                if (!db.isOpen()) {
                    db = mDbHelper.getReadableDatabase();
                }

                mNumTraces = fetchTracesOnMap(db, actIDs, acts, mBuilder);

                long stopFE = System.currentTimeMillis();
                Log.e("MAp FRAGMENT", "Time to load " + count++ + " activities and GPS: " + (stopFE - stopFS));

                long stopLFS = System.currentTimeMillis();
                PAWSActivity prevAct = null;
                for (PAWSActivity act : activities) {
                    if(act.getType() == ActivityRecognizer.ACTIVITY_WALK){
                        mGPSLengthWalk += act.getDistance();
                        mActivityLengthWalk += act.getPointsCount();
                    } else if (act.getType() == ActivityRecognizer.ACTIVITY_RUN) {
                        mGPSLengthRun += act.getDistance();
                        mActivityLengthRun += act.getPointsCount();
                    }

                    // Connect activities if within MAX_TIME_DIFFERENCE_FOR_CONNECTION bounds
                    if (act.getTime() - lastTime < MAX_TIME_DIFFERENCE_FOR_CONNECTION) {
                        LatLng lastPoint = prevAct.getLastPoint();
                        if (lastPoint != null) {
                            act.addPoint(0, lastPoint);
                        }
                    }
                    lastTime = act.getTime();
                    prevAct = act;
                }

                long stopLFE = System.currentTimeMillis();
                Log.e("MAp FRAGMENT", "Time to process activities and GPS: " + (stopLFE - stopLFS));

//                db.close();

                MapFragment.this.mActivities = activities;

                long stopWE = System.currentTimeMillis();
                Log.e("MAp FRAGMENT", "Time to load all activities and GPS: " + (stopWE - stopWS));

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        updateMap();
                    }
                });
            }
        });


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // Draw fetched results on map if available
                if (mActivities != null) {
                    // We got early results!
                    updateMap();
                }
            }
        });

        // Format Xaxis of barchart (bottom)
        YAxis left = mBarChart.getAxisLeft();
        left.setDrawLabels(false);
        left.setDrawAxisLine(false);
        left.setDrawGridLines(false);
        left.setDrawZeroLine(true);
        mBarChart.getAxisRight().setEnabled(false);

        XAxis xAxis = mBarChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(6f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(true);
        xAxis.setGranularity(1f);

        xAxis.setAxisMinimum(0);
        xAxis.setAxisMaximum(23);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return (int) value + "";
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });

        // Disable all interaction but tapping
        mBarChart.setDragEnabled(false);
        mBarChart.setScaleEnabled(false);
        mBarChart.setPinchZoom(false);
        mBarChart.setDoubleTapToZoomEnabled(false);
        mBarChart.setFitBars(true);
        mBarChart.setDescription(null);

        loadBarChartData();

        return rootView;
    }


    /**
     * Loads the barchart data, if activity is still current.
     * Ensures the screen still displays this fragment
     */
    private void loadBarChartData() {
        Activity activity = getActivity();
        if (!isAdded() || activity == null) {
            return;
        }

        BarData data = new BarData(getDataSet(mStartTime, mEndTime));
        data.setBarWidth(barWidth);
        mBarChart.setData(data);
        mBarChart.groupBars(0f, groupSpace, barSpace);
        mBarChart.invalidate();
    }

    /**
     * Gets the data grouped per hour for a given timeframe
     * @param start Start of timeframe
     * @param end End of timeframe
     * @return Dataset compatible with MPAndroidChart
     */
    private ArrayList<IBarDataSet> getDataSet(long start, long end) {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        HashMap<String, float[]> data = getActivityCounts(db, start, end);
//        db.close();

        SimpleDateFormat format = new SimpleDateFormat("HH");
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, 0);

        ArrayList<BarEntry> valuesResting = new ArrayList<>();
        ArrayList<BarEntry> valueWalking = new ArrayList<>();
        ArrayList<BarEntry> valueRunning = new ArrayList<>();

        for (int i = 0; i < 24; ++i) {


            float[] item = data.get(format.format(date.getTime()));
            Log.i(TAG, "DATE=" + format.format(date.getTime()));
            if (item != null) {
                float sum = item[ActivityRecognizer.ACTIVITY_REST] +item[ActivityRecognizer.ACTIVITY_WALK] + item[ActivityRecognizer.ACTIVITY_RUN];
                valuesResting.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_REST]/sum));
                valueWalking.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_WALK]/sum));
                valueRunning.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_RUN]/sum));
                Log.i(TAG, "i=" + i + " " + item[ActivityRecognizer.ACTIVITY_REST]);
            } else {
                valuesResting.add(new BarEntry(i, 0));
                valueWalking.add(new BarEntry(i, 0));
                valueRunning.add(new BarEntry(i, 0));
            }

            date.add(Calendar.HOUR_OF_DAY, 1);
        }

        date.add(Calendar.HOUR_OF_DAY, -24);

        BarDataSet barDataSet1 = new BarDataSet(valuesResting, "Resting");
        barDataSet1.setColor(getResources().getColor(R.color.activity_resting));
        barDataSet1.setDrawValues(false);
        BarDataSet barDataSet2 = new BarDataSet(valueWalking, "Walking");
        barDataSet2.setColor(getResources().getColor(R.color.activity_walking));
        barDataSet2.setDrawValues(false);
        BarDataSet barDataSet3 = new BarDataSet(valueRunning, "Running");
        barDataSet3.setColor(getResources().getColor(R.color.activity_running));
        barDataSet3.setDrawValues(false);


        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        dataSets.add(barDataSet3);
        return dataSets;
    }

    /**
     * Converts an android DP value to pixels
     * @param dp To be converted value
     * @return Pixel value matching the screen dimension
     */
    private int dpToPixels(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }


    private HashMap<String, float[]> getActivityCounts(SQLiteDatabase db, long start, long end) {

        // Used SQL query for binnin timestamps
        //select strftime('%d-%m-%Y', time,'unixepoch') as date, type, COUNT(acc._id) as count from activity LEFT JOIN accelero AS acc ON acc.aid = activity._id where time > 1481068800 group by date, activity.type;

        HashMap<String, float[]> data = new HashMap<>();

        String[] projection = {
                "strftime('%H', " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + ",'unixepoch') as date",
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                "COUNT(acc._id) as count"
        };

        String whereClause = PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + ">? and " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + "<?";
        String[] whereArgs = { start + "", end + ""};

        Cursor c = db.query(
                PAWSDBHelper.ActivityEntry.TABLE_NAME + " LEFT JOIN " + PAWSDBHelper.AcceleroEntry.TABLE_NAME + " AS acc ON acc.aid = " + PAWSDBHelper.ActivityEntry.TABLE_NAME + "." + PAWSDBHelper.ActivityEntry._ID,
                projection,
                whereClause,
                whereArgs,
                "date, " + PAWSDBHelper.ActivityEntry.TABLE_NAME + "." + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                String date = c.getString(c.getColumnIndexOrThrow("date"));
                int type = c.getInt(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE));
                long count = c.getLong(c.getColumnIndexOrThrow("count"));

                float[] item = data.get(date);
                if (item == null) {
                    item = new float[3];
                    data.put(date, item);
                }

                item[type] = count;
            } while (c.moveToNext());
        }

        return data;
    }

    /**
     * Draws traces on the map and updates the overlay values (time and distance)
     */
    private void updateMap() {
        Activity activity = getActivity();
        if (!isAdded() || activity == null) {
            return;
        }
        if (mapUpdated || mActivities == null) {
            return;
        }

        mapUpdated = true;

        // Set labels on overlay
        String timeformatted = TimeFormatter.format((int)mActivityLengthWalk);
        mDurationWalk.setText(timeformatted);

        timeformatted = TimeFormatter.format((int)mActivityLengthRun);
        mDurationRun.setText(timeformatted);

        String distanceFormatted = DistanceFormatter.format(mGPSLengthWalk);
        mDistanceWalk.setText(distanceFormatted);

        distanceFormatted = DistanceFormatter.format(mGPSLengthWalk);
        mDistanceRun.setText(distanceFormatted);

        // Put all traces on the map
        for (PAWSActivity act : mActivities) {
            drawTraceOnMap(act);
        }

        if(mNumTraces > 0) {
            LatLngBounds bounds = mBuilder.build();

            int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.moveCamera(cu);
        }
    }

    /**
     * Draws a signle trace of an activity on the map.
     * @param act Activity containing GPS points
     */
    private void drawTraceOnMap(PAWSActivity act) {
        ArrayList<LatLng> points = act.getPoints();

        if (points != null) {

            Polyline line = googleMap.addPolyline(new PolylineOptions()
                    .addAll(points)
                    .width((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()))
                    .color(getColorByActivityType(act.getType())));
        }
    }

    /**
     *
     * @param db
     * @param acts
     * @param activities
     * @param builder
     * @return Number of traces found in the search query
     */
    private int fetchTracesOnMap(SQLiteDatabase db, String[] acts, HashMap<Long, PAWSActivity> activities, LatLngBounds.Builder builder) {
        String[] projection = {
                PAWSDBHelper.GPSEntry._ID,
                PAWSDBHelper.GPSEntry.COLUMN_NAME_LAT,
                PAWSDBHelper.GPSEntry.COLUMN_NAME_LONG,
                PAWSDBHelper.GPSEntry.COLUMN_NAME_AID
        };

        String whereClause = PAWSDBHelper.GPSEntry.COLUMN_NAME_AID + ">= ? AND " + PAWSDBHelper.GPSEntry.COLUMN_NAME_AID + "<= ? ";

        Cursor cursor = db.query(
                PAWSDBHelper.GPSEntry.TABLE_NAME,
                projection,
                whereClause,
                acts,
                null,
                null,
                "_id ASC"
        );

        long currentAct;
        long prevAct = 0;
        double gpsLat;
        double gpsLong;
        double gpsPrevLat = 0;
        double gpsPrevLong = 0;
        int numberOfTraces = 0;

        float[] results = new float[3];

        if (cursor.moveToFirst()) {
            do {
                currentAct = cursor.getLong(cursor.getColumnIndexOrThrow(PAWSDBHelper.GPSEntry.COLUMN_NAME_AID));
                gpsLat = cursor.getDouble(cursor.getColumnIndexOrThrow(PAWSDBHelper.GPSEntry.COLUMN_NAME_LAT));
                gpsLong = cursor.getDouble(cursor.getColumnIndexOrThrow(PAWSDBHelper.GPSEntry.COLUMN_NAME_LONG));
                LatLng marker = new LatLng(gpsLat, gpsLong);

                PAWSActivity pawsActivity = activities.get(currentAct);

                if(pawsActivity == null){
                    continue;
                }

                if (prevAct == currentAct) {
                    Location.distanceBetween(gpsPrevLat, gpsPrevLong, gpsLat, gpsLong, results);
                    pawsActivity.addDistance(results[0]);
                }

                gpsPrevLat = gpsLat;
                gpsPrevLong = gpsLong;
                prevAct = currentAct;

                pawsActivity.addPoint(marker);
                builder.include(marker);
                numberOfTraces++;
            } while (cursor.moveToNext());
        }

        cursor.close();
        return numberOfTraces;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    /**
     * Creates a list of all activities occured between start and end
     * @param db Database to be used for queries
     * @param start Start argument
     * @param end End argument
     * @return List of PAWSActivity
     */
    private ArrayList<PAWSActivity> getActivitiesBetween(SQLiteDatabase db, long start, long end) {
        String[] projection = {
                PAWSDBHelper.ActivityEntry._ID,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME
        };

        String whereClause = PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + ">? AND " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + " <?";
        String[] whereArgs = {start + "", end + ""};

        Cursor c = db.query(
                PAWSDBHelper.ActivityEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                "_id ASC"
        );

        ArrayList<PAWSActivity> list = null;

        if (c.moveToFirst()) {
            list = new ArrayList<>();

            do {
                long id = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry._ID));
                int type = c.getInt(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE));
                long time = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME));
                PAWSActivity act = new PAWSActivity(id, type, time);
                list.add(act);
            } while (c.moveToNext());
        }


        return list;

    }

    /**
     * Converts activity types to actual RGB colors as defined in the resources
     * @param activityType Activity type as defined in ActivityRecognizer
     * @return RGB color value
     */
    public int getColorByActivityType(int activityType) {
        switch (activityType) {
            case ActivityRecognizer.ACTIVITY_REST:
                return getResources().getColor(R.color.activity_resting);
            case ActivityRecognizer.ACTIVITY_WALK:
                return getResources().getColor(R.color.activity_walking);
            case ActivityRecognizer.ACTIVITY_RUN:
                return getResources().getColor(R.color.activity_running);
        }
        return 0;
    }

}
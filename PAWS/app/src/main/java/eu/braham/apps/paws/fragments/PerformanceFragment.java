package eu.braham.apps.paws.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.DecoDrawEffect;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import eu.braham.apps.paws.MainActivity;
import eu.braham.apps.paws.R;
import eu.braham.apps.paws.database.PAWSActivity;
import eu.braham.apps.paws.database.PAWSDBHelper;
import eu.braham.apps.paws.processor.ActivityRecognizer;

import static android.R.attr.absListViewStyle;
import static android.R.attr.borderlessButtonStyle;
import static android.R.attr.value;

public class PerformanceFragment extends Fragment implements OnChartValueSelectedListener {

    private final String TAG = "PerformanceFragment";

    private final float groupSpace = 0.07f;
    private final float barSpace = 0.01f;
    private final float barWidth = 0.30f;

    private DecoView mDVdynamicArcViewActivity;
    private BarChart mBarChart;
    private TextView mTextActivity1;
    private TextView mTextActivity2;
    private TextView mTextActivity3;

    int mBackIndex;
    int mSeries1Index;
    int mSeries2Index;
    int mSeries3Index;
    int mSeries1bIndex;
    int mSeries2bIndex;
    int mSeries3bIndex;
    int value = 0;

    int mpm;
    int mpm2;
    int mpm3;
    int delay1b;
    int delay2b;
    int delay3b;

    private int mSeriesMaxrest = 30;
    private int mSeriesMaxwalk = 30;
    private int mSeriesMaxrun = 30;

    private int numberOfDays = 5;
    private boolean doAnimations = true;
    private int mScrollX = 0;
    private int mScrollY = 0;

    private OnPerformanceFragmentListener mListener;
    private PAWSDBHelper mDbHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHelper = new PAWSDBHelper(getActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "OnCreateView" + doAnimations);

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_performance, container, false);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        sharedPref.registerOnSharedPreferenceChangeListener(spChanged);

        // Load target settings
        String target = sharedPref.getString("performance_target_rest", "30");
        try {
            mSeriesMaxrest = Integer.parseInt(target);
        } catch (NumberFormatException e){
        }

        target = sharedPref.getString("performance_target_walk", "30");
        try {
            mSeriesMaxwalk = Integer.parseInt(target);
        } catch (NumberFormatException e){
        }

        target = sharedPref.getString("performance_target_run", "30");
        try {
            mSeriesMaxrun = Integer.parseInt(target);
        } catch (NumberFormatException e){
        }


        final ScrollView scrollView = (ScrollView) v.findViewById(R.id.scrollView);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY(); // For ScrollView
                int scrollX = scrollView.getScrollX(); // For HorizontalScrollView
                mScrollX = scrollX;
                mScrollY = scrollY;
            }
        });

        loadHistoryDaysPreference(sharedPref);


        mDVdynamicArcViewActivity = (DecoView) v.findViewById(R.id.DVdynamicArcViewActivity);
        mBarChart = (BarChart) v.findViewById(R.id.chart);
        mTextActivity1 = (TextView) v.findViewById(R.id.textActivity1);
        mTextActivity2 = (TextView) v.findViewById(R.id.textActivity2);
        mTextActivity3 = (TextView) v.findViewById(R.id.textActivity3);


        mDVdynamicArcViewActivity.disableHardwareAccelerationForDecoView();
        createBackSeries();
        createDataSeries1();
//        createDataSeries1b();
        createDataSeries2();
        createDataSeries3();
//        createEvents();


        mBarChart.setDescription(null);
        if (doAnimations) {
            mBarChart.animateY(5000);
        } else {
            mBarChart.animateY(0);
            // Also scroll down to show the chart
            scrollView.setScrollX(mScrollX);
            scrollView.setScrollX(mScrollY);
        }


        YAxis left = mBarChart.getAxisLeft();
        left.setDrawLabels(false);
        left.setDrawAxisLine(false);
        left.setDrawGridLines(false);
        left.setDrawZeroLine(true);
        mBarChart.getAxisRight().setEnabled(false);

        XAxis xAxis = mBarChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setAxisMinimum(0);
        xAxis.setAxisMaximum(numberOfDays);
        xAxis.setCenterAxisLabels(true);

        // Disable all interaction but tapping
        mBarChart.setDragEnabled(false);
        mBarChart.setScaleEnabled(false);
        mBarChart.setPinchZoom(false);
        mBarChart.setDoubleTapToZoomEnabled(false);
        mBarChart.setFitBars(true);

        loadBarChartData();

        mBarChart.setOnChartValueSelectedListener(this);


        doAnimations = false;
        return v;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnPerformanceFragmentListener) {
            mListener = (OnPerformanceFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPerformanceFragmentListener");
        }

        Log.i(TAG, "OnAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void loadHistoryDaysPreference(SharedPreferences sharedPref) {
        String historyDays = sharedPref.getString("performance_history_days", "5");
        try {
            numberOfDays = Integer.valueOf(historyDays);
        } catch (NumberFormatException e) {

        }

        Log.i(TAG, "OnDetach");
    }

    private void loadBarChartData() {
        Activity activity = getActivity();
        if (!isAdded() || activity == null) {
            return;
        }
        BarData data = new BarData(getDataSet());

        XAxis xAxis = mBarChart.getXAxis();
        xAxis.setAxisMinimum(0);
        xAxis.setAxisMaximum(numberOfDays);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                String[] items = getXAxisValues();
                if (value >= 0 && value < items.length) {
                    return items[(int) value];
                }
                return null;
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });

        data.setBarWidth(barWidth);
        mBarChart.setData(data);
        mBarChart.groupBars(0f, groupSpace, barSpace);
        mBarChart.invalidate();

    }


    private void createBackSeries() {
        SeriesItem seriesItem = new SeriesItem.Builder(getResources().getColor(R.color.activity_background))
                .setRange(0, 30, 0)
                .setLineWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics()))
                .setInitialVisibility(true)
                .setInset(new PointF(dpToPixels(30), dpToPixels(30)))
                .build();

        mBackIndex = mDVdynamicArcViewActivity.addSeries(seriesItem);
    }

    private void createDataSeries1() {
        final SeriesItem seriesItem = new SeriesItem.Builder(getResources().getColor(R.color.activity_resting))//, (R.color.activity_resting))
                .setRange(0, mSeriesMaxrest, 0)
                .setInset(new PointF(dpToPixels(10), dpToPixels(10)))
                .setInitialVisibility(false)
                .build();

        seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                mTextActivity1.setText(String.format("%.0f minutes", currentPosition));
            }


            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        mSeries1Index = mDVdynamicArcViewActivity.addSeries(seriesItem);

    }

    private float createDataSeries1b(float restvalue) {

        if (restvalue > mSeriesMaxrest)
        {
            float value1b = restvalue % mSeriesMaxrest;

            mpm = Math.round(2500 / mSeriesMaxrest); // movement per millisecond
            delay1b = ((mSeriesMaxrest * mpm) + 140);


            final SeriesItem seriesItem = new SeriesItem.Builder(getResources().getColor(R.color.activity_resting2), R.color.activity_resting)
                    .setRange(0, mSeriesMaxrest, 0)
                    .setInset(new PointF(dpToPixels(10), dpToPixels(10)))
                    .setInitialVisibility(false)
                    .build();

            seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
                @Override
                public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                }

                @Override
                public void onSeriesItemDisplayProgress(float percentComplete) {

                }
            });

            mSeries1bIndex = mDVdynamicArcViewActivity.addSeries(seriesItem);

            return value1b;
        }

        return 0;
    }

    private void createDataSeries2() {
        final SeriesItem seriesItem = new SeriesItem.Builder(getResources().getColor(R.color.activity_walking))
                .setRange(0, mSeriesMaxwalk, 0)
                .setInitialVisibility(false)
                .setInset(new PointF(dpToPixels(30), dpToPixels(30)))
                .build();

        seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                mTextActivity2.setText(String.format("%.0f minutes", currentPosition));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        mSeries2Index = mDVdynamicArcViewActivity.addSeries(seriesItem);
    }

    private float createDataSeries2b(float restvalue) {

        if (restvalue > mSeriesMaxwalk)
        {
            float value2b = restvalue % mSeriesMaxwalk;

            mpm2 = Math.round(2500 / mSeriesMaxwalk); // movement per millisecond
            delay2b = ((mSeriesMaxwalk * mpm2) + 140);


            final SeriesItem seriesItem = new SeriesItem.Builder(getResources().getColor(R.color.activity_resting2), R.color.activity_resting)
                    .setRange(0, mSeriesMaxwalk, 0)
                    .setInset(new PointF(dpToPixels(30), dpToPixels(30)))
                    .setInitialVisibility(false)
                    .build();

            seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
                @Override
                public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                }

                @Override
                public void onSeriesItemDisplayProgress(float percentComplete) {

                }
            });

            mSeries2bIndex = mDVdynamicArcViewActivity.addSeries(seriesItem);

            return value2b;
        }

        return 0;
    }

    private void createDataSeries3() {
        final SeriesItem seriesItem = new SeriesItem.Builder(getResources().getColor(R.color.activity_running))
                .setRange(0, mSeriesMaxrun, 0)
                .setInitialVisibility(false)
                .setInset(new PointF(dpToPixels(50), dpToPixels(50)))
                .build();


        seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                mTextActivity3.setText(String.format("%.2f minutes", currentPosition));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        mSeries3Index = mDVdynamicArcViewActivity.addSeries(seriesItem);
    }

    private float createDataSeries3b(float restvalue) {

        if (restvalue > mSeriesMaxrun)
        {
            float value3b = restvalue % mSeriesMaxrun;

            mpm3 = Math.round(2500 / mSeriesMaxrun); // movement per millisecond
            delay1b = ((mSeriesMaxrun * mpm3) + 140);


            final SeriesItem seriesItem = new SeriesItem.Builder(getResources().getColor(R.color.activity_resting2), R.color.activity_resting)
                    .setRange(0, mSeriesMaxrun, 0)
                    .setInset(new PointF(dpToPixels(50), dpToPixels(50)))
                    .setInitialVisibility(false)
                    .build();

            seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
                @Override
                public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                }

                @Override
                public void onSeriesItemDisplayProgress(float percentComplete) {

                }
            });

            mSeries3bIndex = mDVdynamicArcViewActivity.addSeries(seriesItem);

            return value3b;
        }

        return 0;
    }

    private void createEvents(float rest, float walk, float run) {
        mDVdynamicArcViewActivity.executeReset();

        long animationDelay = 1000;
        long animationDuration = 2500;

        if(!doAnimations){
            animationDelay = 0;
            animationDuration = 0;
        }

        // check overflow

        float value1b = createDataSeries1b(rest);
        float value2b = createDataSeries2b(walk);
        float value3b = createDataSeries3b(run);

        // Draw background
        mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(30)
                .setIndex(mBackIndex)
                .setDuration(0)
                .build());

        mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(DecoDrawEffect.EffectType.EFFECT_SPIRAL_OUT)
                .setIndex(mSeries1Index)
                .setDuration(0)
                .setDelay(0)
                .build());

        // Resting animation
        mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(rest)
                .setIndex(mSeries1Index)
                .setDelay(animationDelay)
                .setDuration(animationDuration)
                .build());

        if(value1b > 0) {
            mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(DecoDrawEffect.EffectType.EFFECT_SPIRAL_OUT)
                    .setIndex(mSeries1bIndex)
                    .setDuration(0)
                    .setDelay(delay1b)
                    .build());

            mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(value1b)
                    .setIndex(mSeries1bIndex)
                    .setDelay(delay1b)
                    .setDuration(mpm)
                    .build());
        }

        // Walking animation
        mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(DecoDrawEffect.EffectType.EFFECT_SPIRAL_OUT)
                .setIndex(mSeries2Index)
                .setDuration(0)
                .setDelay(0)
                .build());

        mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(walk)
                .setIndex(mSeries2Index)
                .setDelay(animationDelay)
                .setDuration(animationDuration)
                .build());

        if(value2b > 0) {
            mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(DecoDrawEffect.EffectType.EFFECT_SPIRAL_OUT)
                    .setIndex(mSeries2bIndex)
                    .setDuration(0)
                    .setDelay(delay2b)
                    .build());

            mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(value2b)
                    .setIndex(mSeries2bIndex)
                    .setDelay(delay2b)
                    .setDuration(mpm2)
                    .build());
        }

        // Running animation
        mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(DecoDrawEffect.EffectType.EFFECT_SPIRAL_OUT)
                .setIndex(mSeries3Index)
                .setDuration(0)
                .setDelay(0)
                .build());

        mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(run)
                .setIndex(mSeries3Index)
                .setDelay(animationDelay)
                .setDuration(animationDuration)
                .build());

        if(value3b > 0) {
            mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(DecoDrawEffect.EffectType.EFFECT_SPIRAL_OUT)
                    .setIndex(mSeries3bIndex)
                    .setDuration(0)
                    .setDelay(delay3b)
                    .build());

            mDVdynamicArcViewActivity.addEvent(new DecoEvent.Builder(value3b)
                    .setIndex(mSeries3bIndex)
                    .setDelay(delay3b)
                    .setDuration(mpm3)
                    .build());
        }
    }


    private ArrayList<IBarDataSet> getDataSet() {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        HashMap<String, float[]> data = getActivityCounts(db, 0);
//        db.close();

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE, -numberOfDays);

        ArrayList<BarEntry> valuesResting = new ArrayList<>();
        ArrayList<BarEntry> valueWalking = new ArrayList<>();
        ArrayList<BarEntry> valueRunning = new ArrayList<>();

        for (int i = 0; i < numberOfDays; ++i) {
            date.add(Calendar.DATE, 1);

            float[] item = data.get(format.format(date.getTime()));
            Log.i(TAG, "DATE=" + format.format(date.getTime()));
            if (item != null) {
                float sum = item[ActivityRecognizer.ACTIVITY_REST] +item[ActivityRecognizer.ACTIVITY_WALK] + item[ActivityRecognizer.ACTIVITY_RUN];
                valuesResting.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_REST]/sum));
                valueWalking.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_WALK]/sum));
                valueRunning.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_RUN]/sum));
                Log.i(TAG, "i=" + i + " " + item[ActivityRecognizer.ACTIVITY_REST]);
            } else {
                valuesResting.add(new BarEntry(i, 0));
                valueWalking.add(new BarEntry(i, 0));
                valueRunning.add(new BarEntry(i, 0));
            }
        }



        // Update decoview
        float[] item = data.get(format.format(date.getTime()));
        Log.i(TAG, "DECOVIEW DATE=" + format.format(date.getTime()));
        if (item != null) {
            createEvents(item[ActivityRecognizer.ACTIVITY_REST] /60, item[ActivityRecognizer.ACTIVITY_WALK] /60, item[ActivityRecognizer.ACTIVITY_RUN] /60);
        } else {
            createEvents(0, 0, 0);
        }

        BarDataSet barDataSet1 = new BarDataSet(valuesResting, "Resting");
        barDataSet1.setColor(getResources().getColor(R.color.activity_resting));
        barDataSet1.setDrawValues(false);
        BarDataSet barDataSet2 = new BarDataSet(valueWalking, "Walking");
        barDataSet2.setColor(getResources().getColor(R.color.activity_walking));
        barDataSet2.setDrawValues(false);
        BarDataSet barDataSet3 = new BarDataSet(valueRunning, "Running");
        barDataSet3.setColor(getResources().getColor(R.color.activity_running));
        barDataSet3.setDrawValues(false);


        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        dataSets.add(barDataSet3);
        return dataSets;

    }


    private String[] getXAxisValues() {
        SimpleDateFormat format = new SimpleDateFormat("E");
        Calendar date = Calendar.getInstance();
        String[] xAxis = new String[numberOfDays];


        for (int i = numberOfDays - 1; i >= 0; --i) {
            xAxis[i] = format.format(date.getTime());
            date.add(Calendar.DATE, -1);
        }

        return xAxis;
    }

    private int dpToPixels(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }


    private HashMap<String, float[]> getActivityCounts(SQLiteDatabase db, long start) {

        //select strftime('%d-%m-%Y', time,'unixepoch') as date, type, COUNT(acc._id) as count from activity LEFT JOIN accelero AS acc ON acc.aid = activity._id where time > 1481068800 group by date, activity.type;

        HashMap<String, float[]> data = new HashMap<>();

        long swstart = System.currentTimeMillis();

        String[] projection = {
                "strftime('%d-%m-%Y', " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + ",'unixepoch') as date",
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                "COUNT(acc._id) as count"
        };

        String whereClause = PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + ">? ";
        String[] whereArgs = {start + ""};

        Cursor c = db.query(
                PAWSDBHelper.ActivityEntry.TABLE_NAME + " LEFT JOIN " + PAWSDBHelper.AcceleroEntry.TABLE_NAME + " AS acc ON acc.aid = " + PAWSDBHelper.ActivityEntry.TABLE_NAME + "." + PAWSDBHelper.ActivityEntry._ID,
                projection,
                whereClause,
                whereArgs,
                "date, " + PAWSDBHelper.ActivityEntry.TABLE_NAME + "." + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                String date = c.getString(c.getColumnIndexOrThrow("date"));
                int type = c.getInt(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE));
                long count = c.getLong(c.getColumnIndexOrThrow("count"));

                float[] item = data.get(date);
                if (item == null) {
                    item = new float[3];
                    data.put(date, item);
                }

                item[type] = count;

                Log.i(TAG, "Got row " + date + " " + type + " " + count);
            } while (c.moveToNext());
        }

        long end = System.currentTimeMillis();
        Log.e(TAG, "Time: " + (end - swstart));

        return data;

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (h == null)
            return;

        int x = (int) Math.floor(h.getX());

        // Find the date corresponding with the X location
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE, -numberOfDays + x + 1);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        long start = date.getTimeInMillis() / 1000;
        date.add(Calendar.DATE, 1);
        long end = date.getTimeInMillis() / 1000;

        // Inform MainActivity to load details page
        if (mListener != null) {
            mListener.onTimeSelected(start, end);
        }
    }

    @Override
    public void onNothingSelected() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnPerformanceFragmentListener {
        void onTimeSelected(long start, long end);
    }

    SharedPreferences.OnSharedPreferenceChangeListener spChanged = new
            SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                                      String key) {
                    Log.i(TAG, "Got pref change for " + key);

                    if (key.equals("performance_history_days")) {
                        loadHistoryDaysPreference(sharedPreferences);
                        loadBarChartData();
                    }

                    // TODO:
//                    performance_target_rest
//                    performance_target_walk
//                    performance_target_run
                }
            };
}
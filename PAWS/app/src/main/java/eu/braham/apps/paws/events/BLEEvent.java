package eu.braham.apps.paws.events;

/**
 * Created by kbraham on 11/6/16.
 */

public class BLEEvent {

    private String message;

    public BLEEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

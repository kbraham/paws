package eu.braham.apps.paws.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DateFormat;
import java.util.Date;

import eu.braham.apps.paws.database.PAWSDBHelper;
import eu.braham.apps.paws.events.AcceleroEvent;
import eu.braham.apps.paws.events.ActivityEvent;
import eu.braham.apps.paws.events.BLEEvent;
import eu.braham.apps.paws.events.BufferRequestEvent;
import eu.braham.apps.paws.events.BufferResponseEvent;
import eu.braham.apps.paws.events.GPSEvent;
import eu.braham.apps.paws.events.GyroEvent;
import eu.braham.apps.paws.processor.ActivityRecognizer;
import eu.braham.apps.paws.processor.CircularQueue;

import static eu.braham.apps.paws.services.UbloxOfflineService.TAG;


/**
 * Created by coen on 10/11/2016.
 */

public class SensorsService extends Service {


    public final static int MAX_ACCELERO_EVENTS = 300;
    public final static int MIN_STATE_STABLE_TIME = 5000;

    private ActivityRecognizer mActivityRecognizer = new ActivityRecognizer();
    private CircularQueue<AcceleroEvent> mAccQueue = new CircularQueue<>(MAX_ACCELERO_EVENTS);
    private int oldstate = mActivityRecognizer.ACTIVITY_REST;
    private long stateSwitchTime;
    private boolean canStartNewRecording = false;

    private boolean mRecording = true;
    private long mActivityID = -1;
    private PAWSDBHelper mDbHelper;


    private final BroadcastReceiver TagStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(TagService.ACTION_DATA_AVAILABLE)) {

                // Periodically check state changes, and start new activity on change.
                if(canStartNewRecording && System.currentTimeMillis() - stateSwitchTime > MIN_STATE_STABLE_TIME){
                    Log.e("SensorService", "STarting new activity recoding");
                    canStartNewRecording = false;

                    addActivity("Autorec",  oldstate, System.currentTimeMillis() / 1000, 300);
                }

                final byte[] txValue = intent.getByteArrayExtra(TagService.EXTRA_DATA);
                try {
                    String text = new String(txValue, "UTF-8");

                    // Check first letter to parse messages
                    // A for accelero, G for gyro and L for location updates
                    if (text.charAt(0) == 'A') {
                        AcceleroEvent ae = AcceleroEvent.parseText(text);
                        EventBus.getDefault().post(ae);
                        mAccQueue.insert(ae);
                        int state = mActivityRecognizer.getCurrentActivityType(mAccQueue);
                        if (state != oldstate) {
                            ActivityEvent ace = new ActivityEvent(state);
                            EventBus.getDefault().post(ace);
                            oldstate = state;
                            stateSwitchTime = System.currentTimeMillis();
                            canStartNewRecording = true;
                        }
                        Log.i(TAG, ae.toString());
                        addAcceleroEvent(ae);
                    } else if (text.charAt(0) == 'G') {
                        GyroEvent ge = GyroEvent.parseText(text);
                        EventBus.getDefault().post(ge);
                        Log.i(TAG, ge.toString());
                    } else if (text.charAt(0) == 'L' || text.endsWith("E") || text.endsWith("W")) {
                        GPSEvent ge = GPSEvent.parseText(text);
                        if (ge != null) {
                            EventBus.getDefault().post(ge);
                            Log.i(TAG, ge.toString());
                            addGPSEvent(ge);
                        }
                    }
                    String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                    EventBus.getDefault().post(new BLEEvent("> " + currentDateTimeString + " " + text));

                    Log.d(TAG, "BLE " + text);

                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }

        }
    };


    //// Database log methods

    /**
     * Starts a new activity in the database
     * @param type Activity type
     * @param time Start time
     * @param duration Duration (probably not yet known here)
     */
    private void addActivity(int type, long time, int duration) {
        addActivity("", type, time, duration);
    }

    private void addActivity(String name, int type, long time, int duration) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_NAME, name);
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE, type);
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME, time);
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_DURATION, duration);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(PAWSDBHelper.ActivityEntry.TABLE_NAME, null, values);

        mActivityID = newRowId;

        if (newRowId > -1) {
            Log.i(TAG, "Succesfully added a row!");
        }

//        db.close();
    }

    private void addAcceleroEvent(AcceleroEvent ai) {
        if (mRecording && mActivityID > -1) {
            // Gets the data repository in write mode
            SQLiteDatabase db = mDbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_AID, mActivityID);
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_X, ai.getX());
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Y, ai.getY());
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Z, ai.getZ());
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_TIMESTAMP, System.currentTimeMillis() / 1000);

            long newRowId = db.insert(PAWSDBHelper.AcceleroEntry.TABLE_NAME, null, values);

            if (newRowId > -1) {
                Log.i(TAG, "Successfully added acc row!");
            }

//            db.close();
        }
    }

    private void addGPSEvent(GPSEvent gpsEvent) {
        if (mRecording && mActivityID > -1 && gpsEvent.getStatus() == GPSEvent.STATUS_FIX) {
            // Gets the data repository in write mode
            SQLiteDatabase db = mDbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(PAWSDBHelper.GPSEntry.COLUMN_NAME_AID, mActivityID);
            values.put(PAWSDBHelper.GPSEntry.COLUMN_NAME_LAT, gpsEvent.getLat());
            values.put(PAWSDBHelper.GPSEntry.COLUMN_NAME_LONG, gpsEvent.getLon());
            values.put(PAWSDBHelper.GPSEntry.COLUMN_NAME_TIMESTAMP, System.currentTimeMillis() / 1000);

            long newRowId = db.insert(PAWSDBHelper.GPSEntry.TABLE_NAME, null, values);

            if (newRowId > -1) {
                Log.i(TAG, "Successfully added gps row!");
            }

//            db.close();
        }
    }


    private void serviceInit() {
        EventBus.getDefault().register(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(TagStatusChangeReceiver, makeGattUpdateIntentFilter());

        // Database play stuff
        mDbHelper = new PAWSDBHelper(this);
    }

    private void serviceStop() {
        EventBus.getDefault().unregister(this);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(TagStatusChangeReceiver);
    }
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TagService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    public class LocalBinder extends Binder {
        public SensorsService getService() {
            return SensorsService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        serviceInit();
        return mBinder;
    }


    @Override
    public boolean onUnbind(Intent intent) {
        serviceStop();
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new SensorsService.LocalBinder();


    @Subscribe
    public void onMessage(BufferRequestEvent event) {
        BufferResponseEvent bre = new BufferResponseEvent(mAccQueue);
        EventBus.getDefault().post(bre);
    }
}
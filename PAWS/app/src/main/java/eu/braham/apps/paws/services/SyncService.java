package eu.braham.apps.paws.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import com.intesens.kinto_http.ClientException;
import com.intesens.kinto_http.Collection;
import com.intesens.kinto_http.KintoClient;
import com.intesens.kinto_http.KintoException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.braham.apps.paws.database.PAWSDBHelper;


public class SyncService extends IntentService {
    public static final String TAG = "SYNC";
    public static final String SYNC_START = "eu.braham.apps.paws.services.SyncService.START_SYNC";
    public static final String SYNC_COMPLETE = "eu.braham.apps.paws.services.SyncService.SYNC_COMPLETE";

    private static final int SYNC_MAX_UPLOADS = 50;

    private final static String JSON_ACTIVITY_ID = "paws_id";
    private final static String JSON_ACTIVITY_NAME = "name";
    private final static String JSON_ACTIVITY_TYPE = "type";
    private final static String JSON_ACTIVITY_TIME = "time";
    private final static String JSON_ACTIVITY_DURATION = "duration";
    private final static String JSON_ACTIVITY_KINTO_ID = "id";
    private final static String JSON_ACTIVITY_KINTO_LAST_MODIFIED = "last_modified";
    private final static String JSON_ACTIVITY_ACCELERO = "accel";
    private final static String JSON_ACTIVITY_GPS = "gps";

    private final static String JSON_ACCELERO_X = "x";
    private final static String JSON_ACCELERO_Y = "y";
    private final static String JSON_ACCELERO_Z = "z";
    private final static String JSON_ACCELERO_TIMESTAMP = "timestamp";

    private final static String JSON_GPS_LAT = "lat";
    private final static String JSON_GPS_LONG = "lng";
    private final static String JSON_GPS_TIMESTAMP = "timestamp";

    private PAWSDBHelper mDbHelper;

    public SyncService() {
        super("paws sync service");
        mDbHelper = new PAWSDBHelper(this);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        //TODO: Enable sync
        return;

//        KintoClient kintoClient = new KintoClient("http://192.168.11.54:8888/v1");
//        kintoClient.setAuth("kb", "kb");
//
//        SQLiteDatabase db = mDbHelper.getWritableDatabase();
//
//        try {
//            long time = getLastSyncTime();
//            Log.e(TAG, "SYNTIME" + time);
//            Collection collection = kintoClient.bucket("paws").collection("activities");
//
//            if(!db.isOpen()){
//                db = mDbHelper.getWritableDatabase();
//            }
//
//            List<JSONObject> records = collection.listRecordsSince(time);
//            for (JSONObject json : records) {
//                // Skip all deleted records
//                if (json.has("deleted")) {
//                    continue;
//                }
//
//                // Verify record has all items
//                if (json.has("name") && json.has("type") && json.has("time") /*&& json.has("accel")*/ && json.has("gps")) {
//                    // Insert locally if not exists
//                    if (!hasActivity(db, json)) {
//
//                        Log.i(TAG, "Found new tactivity");
//                        try {
//                            db.beginTransaction();
//                            long r = insertActivity(db, json);
//                            db.setTransactionSuccessful();
//                        } catch (JSONException e) {
//                            Log.e(TAG, e.toString());
//                        } finally {
//                            db.endTransaction();
//                        }
//
//                    }
//                }
//            }
//            records = null;
//
//            if(!db.isOpen()){
//                db = mDbHelper.getWritableDatabase();
//            }
//
//            // Upload all local new records
//            ArrayList<JSONObject> activities = getLocalActivities(db);
//            if(activities != null) {
//                for (JSONObject activity : activities) {
//                    long paws_id = (long) activity.remove(JSON_ACTIVITY_ID);
//
//                    JsonReader reader = collection.findRecordsStream("time="+activity.getLong(JSON_ACTIVITY_TIME)+"&type="+activity.getInt(JSON_ACTIVITY_TYPE));
//                    Pair<String,String> details = getResultID(reader);
//                    reader.close();
//
//                    if(details == null){
//                        activity.put(JSON_ACTIVITY_ACCELERO, getAcceleroForActivity(db, paws_id));
//                        activity.put(JSON_ACTIVITY_GPS, getGPSForActivity(db, paws_id));
//
//                        // Do upload?
//                        Log.i(TAG, "Uploading id " + paws_id);
//
//                        reader = collection.createRecordStream(activity);
//                        Pair<String,String> item = getResultID(reader);
//                        reader.close();
//
//
//                        // Store updated record locally
//                        updateActivity(db, paws_id, item.first, item.second);
//
//                        // Free memory of the large sets
//                        activity.remove(JSON_ACTIVITY_ACCELERO);
//                        activity.remove(JSON_ACTIVITY_GPS);
//                    } else {
//                        // Record exists, do update
//                        Log.i(TAG, "Added id `"+details.first+"` to existing record " + paws_id);
//                        updateActivity(db, paws_id, details.first, details.second);
//                    }
//
//                }
//            } else {
//                Log.d(TAG, "onHandleIntent: No local activities");
//            }
//
//        } catch (KintoException e) {
//            e.printStackTrace();
//        } catch (ClientException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//  //          db.close();
//        }
//
//        Intent intentNotification = new Intent(SYNC_COMPLETE);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(intentNotification);
    }


    // DATABASE QUERIES

    private long getLastSyncTime() {
        long time;

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        SQLiteStatement s = db.compileStatement("select MAX(" + PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_LAST_MODIFIED + ") from " + PAWSDBHelper.ActivityEntry.TABLE_NAME + " ; ");
        time = s.simpleQueryForLong();

//        db.close();
        return time;
    }

    private boolean hasActivity(SQLiteDatabase db, JSONObject act) throws JSONException {
        String selectString = "SELECT " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_ID + " FROM " + PAWSDBHelper.ActivityEntry.TABLE_NAME + " WHERE " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_ID + " =? OR " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME +" = ? AND " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE + " =?";

        // Add the String you are searching by here.
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = db.rawQuery(selectString, new String[]{act.getString(JSON_ACTIVITY_KINTO_ID), act.getLong(JSON_ACTIVITY_TIME) + "", act.getInt(JSON_ACTIVITY_TYPE) + ""});

        boolean hasObject = false;
        if (cursor.moveToFirst()) {
            hasObject = true;
        }

        cursor.close();
        return hasObject;
    }

    private long insertActivity(SQLiteDatabase db, JSONObject activity) throws JSONException {
        ContentValues values = new ContentValues();
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_NAME, activity.getString(JSON_ACTIVITY_NAME));
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE, activity.getInt(JSON_ACTIVITY_TYPE));
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME, activity.getLong(JSON_ACTIVITY_TIME));
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_DURATION, activity.getInt(JSON_ACTIVITY_DURATION));
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_ID, activity.getString(JSON_ACTIVITY_KINTO_ID));
        values.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_LAST_MODIFIED, activity.getString(JSON_ACTIVITY_KINTO_LAST_MODIFIED));

        long result = db.insert(PAWSDBHelper.ActivityEntry.TABLE_NAME, null, values);

        insertAccelero(db, result, activity.getJSONArray(JSON_ACTIVITY_ACCELERO));
        insertGPS(db, result, activity.getJSONArray(JSON_ACTIVITY_GPS));

        return result;
    }

    private void insertAccelero(SQLiteDatabase db, long id, JSONArray list) throws JSONException {

        for(int i = 0; i < list.length(); ++i){
            JSONObject row = list.getJSONObject(i);

            ContentValues values = new ContentValues();
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_AID, id);
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_X, row.getDouble(JSON_ACCELERO_X));
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Y, row.getDouble(JSON_ACCELERO_Y));
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Z, row.getDouble(JSON_ACCELERO_Z));
            values.put(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_TIMESTAMP, row.getLong(JSON_ACCELERO_TIMESTAMP));

            db.insert(PAWSDBHelper.AcceleroEntry.TABLE_NAME, null, values);
        }
    }

    private void insertGPS(SQLiteDatabase db, long id, JSONArray list) throws JSONException {

        for(int i = 0; i < list.length(); ++i){
            JSONObject row = list.getJSONObject(i);

            ContentValues values = new ContentValues();
            values.put(PAWSDBHelper.GPSEntry.COLUMN_NAME_AID, id);
            values.put(PAWSDBHelper.GPSEntry.COLUMN_NAME_LAT, row.getDouble(JSON_GPS_LAT));
            values.put(PAWSDBHelper.GPSEntry.COLUMN_NAME_LONG, row.getDouble(JSON_GPS_LONG));
            values.put(PAWSDBHelper.GPSEntry.COLUMN_NAME_TIMESTAMP, row.getDouble(JSON_GPS_TIMESTAMP));

            db.insert(PAWSDBHelper.GPSEntry.TABLE_NAME, null, values);
        }
    }

    private ArrayList<JSONObject> getLocalActivities(SQLiteDatabase db) throws JSONException {
        String[] projection = {
                PAWSDBHelper.ActivityEntry._ID,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_NAME,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME,
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_DURATION
        };

        String whereClause = PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_LAST_MODIFIED + " IS NULL OR "+ PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_LAST_MODIFIED + " = ''";

        Cursor c = db.query(
                PAWSDBHelper.ActivityEntry.TABLE_NAME,
                projection,
                whereClause,
                null,
                null,
                null,
                "_id ASC",
                "" + SYNC_MAX_UPLOADS
        );

        ArrayList<JSONObject> list = null;

        if (c.moveToFirst()) {
            list = new ArrayList<>();

            do {
                long id = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry._ID));
                String name = c.getString(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_NAME));
                int type = c.getInt(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE));
                long time = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME));
                long duration = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_DURATION));

                JSONObject act = new JSONObject();
                act.put(JSON_ACTIVITY_NAME, name);
                act.put(JSON_ACTIVITY_TYPE, type);
                act.put(JSON_ACTIVITY_TIME, time);
                act.put(JSON_ACTIVITY_DURATION, duration);
                act.put(JSON_ACTIVITY_ID, id);
                list.add(act);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    private JSONArray getAcceleroForActivity(SQLiteDatabase db, long id) throws JSONException {
        JSONArray list = new JSONArray();

        String[] projection = {
                PAWSDBHelper.AcceleroEntry.COLUMN_NAME_X,
                PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Y,
                PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Z,
                PAWSDBHelper.AcceleroEntry.COLUMN_NAME_TIMESTAMP,
        };

        String whereClause = PAWSDBHelper.AcceleroEntry.COLUMN_NAME_AID + " =? ";
        String[] whereArgs = {"" + id};

        Cursor c = db.query(
                PAWSDBHelper.AcceleroEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                "_id ASC"
        );

        if (c.moveToFirst()) {
            do {
                float x, y, z;
                long timestamp;
                x = c.getFloat(c.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_X));
                y = c.getFloat(c.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Y));
                z = c.getFloat(c.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_Z));
                timestamp = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.AcceleroEntry.COLUMN_NAME_TIMESTAMP));

                JSONObject row = new JSONObject();
                row.put(JSON_ACCELERO_X, x);
                row.put(JSON_ACCELERO_Y, y);
                row.put(JSON_ACCELERO_Z, z);
                row.put(JSON_ACCELERO_TIMESTAMP, timestamp);
                list.put(row);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    private JSONArray getGPSForActivity(SQLiteDatabase db, long id) throws JSONException {
        JSONArray list = new JSONArray();

        String[] projection = {
                PAWSDBHelper.GPSEntry.COLUMN_NAME_LAT,
                PAWSDBHelper.GPSEntry.COLUMN_NAME_LONG,
                PAWSDBHelper.GPSEntry.COLUMN_NAME_TIMESTAMP,
        };

        String whereClause = PAWSDBHelper.AcceleroEntry.COLUMN_NAME_AID + " =? ";
        String[] whereArgs = {"" + id};

        Cursor c = db.query(
                PAWSDBHelper.GPSEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                "_id ASC"
        );

        if (c.moveToFirst()) {
            do {
                double lat,lng;
                long timestamp;
                lat = c.getDouble(c.getColumnIndexOrThrow(PAWSDBHelper.GPSEntry.COLUMN_NAME_LAT));
                lng = c.getDouble(c.getColumnIndexOrThrow(PAWSDBHelper.GPSEntry.COLUMN_NAME_LONG));
                timestamp = c.getLong(c.getColumnIndexOrThrow(PAWSDBHelper.GPSEntry.COLUMN_NAME_TIMESTAMP));

                JSONObject row = new JSONObject();
                row.put(JSON_GPS_LAT, lat);
                row.put(JSON_GPS_LONG, lng);
                row.put(JSON_GPS_TIMESTAMP, timestamp);
                list.put(row);
            } while (c.moveToNext());
        }

        c.close();

        return list;
    }

    private boolean updateActivity(SQLiteDatabase db, long id, String kintoID, String timestamp) {
        ContentValues cv = new ContentValues();
        cv.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_ID, kintoID);
        cv.put(PAWSDBHelper.ActivityEntry.COLUMN_NAME_KINTO_LAST_MODIFIED, timestamp);

        return db.update(PAWSDBHelper.ActivityEntry.TABLE_NAME, cv, "_id=" + id, null) > 0;
    }

    private Pair<String, String> getResultID(JsonReader reader) throws IOException {
        reader.beginObject();

        // Look for the data keyword
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("data")) {

                if(reader.peek() == JsonToken.BEGIN_ARRAY){
                    reader.beginArray();
                }

                if(reader.hasNext()){
                    reader.beginObject();
                    // Locate the last_modified and id tags
                    String id = "";
                    String last_modified = "";

                    while(reader.hasNext()){
                        name = reader.nextName();
                        if(name.equals(JSON_ACTIVITY_KINTO_LAST_MODIFIED)){
                            last_modified = reader.nextString();
                        } else if (name.equals(JSON_ACTIVITY_KINTO_ID)){
                            id = reader.nextString();
                        } else {
                            reader.skipValue();
                        }
                    }
                    return new Pair<>(id, last_modified);
                }
            }
        }
        return null;
    }
}
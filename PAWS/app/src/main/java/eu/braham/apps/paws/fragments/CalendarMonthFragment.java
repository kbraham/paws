package eu.braham.apps.paws.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.hookedonplay.decoviewlib.DecoView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import eu.braham.apps.paws.R;
import eu.braham.apps.paws.database.PAWSDBHelper;
import eu.braham.apps.paws.processor.ActivityRecognizer;


public class CalendarMonthFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private final String TAG = "CalendarMonthFragment";

    private BarChart mBarChartw1;
    private BarChart mBarChartw2;
    private BarChart mBarChartw3;
    private BarChart mBarChartw4;

    private int mScrollX = 0;
    private int mScrollY = 0;

    private final float groupSpace = 0.07f;
    private final float barSpace = 0.01f;
    private final float barWidth = 0.30f;

    private PerformanceFragment.OnPerformanceFragmentListener mListener;
    private PAWSDBHelper mDbHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHelper = new PAWSDBHelper(getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof PerformanceFragment.OnPerformanceFragmentListener) {
            mListener = (PerformanceFragment.OnPerformanceFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPerformanceFragmentListener");
        }

        Log.i(TAG, "OnAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_calendar_month, container, false);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//        sharedPref.registerOnSharedPreferenceChangeListener(spChanged);


//        final ScrollView scrollView = (ScrollView) v.findViewById(R.id.scrollView);
//        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                int scrollY = scrollView.getScrollY(); // For ScrollView
//                int scrollX = scrollView.getScrollX(); // For HorizontalScrollView
//                mScrollX = scrollX;
//                mScrollY = scrollY;
//            }
//        });


        mBarChartw1 = (BarChart) v.findViewById(R.id.chartw1);
        mBarChartw2 = (BarChart) v.findViewById(R.id.chartw2);
        mBarChartw3 = (BarChart) v.findViewById(R.id.chartw3);
        mBarChartw4 = (BarChart) v.findViewById(R.id.chartw4);


        configureBarchart(mBarChartw1, -7);
        configureBarchart(mBarChartw2, -14);
        configureBarchart(mBarChartw3, -21);
        configureBarchart(mBarChartw4, -28);

        HashMap<String, float[]> data = getDailyData();

        ArrayList<IBarDataSet> dataset = getDataSet(data, 7);
        loadBarChartData(mBarChartw1, dataset);

        dataset = getDataSet(data, 14);
        loadBarChartData(mBarChartw2, dataset);

        dataset = getDataSet(data, 21);
        loadBarChartData(mBarChartw3, dataset);

        dataset = getDataSet(data, 28);
        loadBarChartData(mBarChartw4, dataset);

        return v;
    }
//
    private void configureBarchart(BarChart barChart, final int daysOffset){
        barChart.setDescription(null);
        barChart.animateY(0);

        YAxis left = barChart.getAxisLeft();
        left.setDrawLabels(false);
        left.setDrawAxisLine(false);
        left.setDrawGridLines(false);
        left.setDrawZeroLine(true);
        barChart.getAxisRight().setEnabled(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setAxisMinimum(0);
        xAxis.setAxisMaximum(7);
        xAxis.setCenterAxisLabels(true);

        // Disable all interaction but tapping
        barChart.setDragEnabled(false);
        barChart.setScaleEnabled(false);
        barChart.setPinchZoom(false);
        barChart.setDoubleTapToZoomEnabled(false);
        barChart.setFitBars(true);

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (h == null)
                    return;
                int x = (int) Math.floor(h.getX());

                openDetailView(x, daysOffset);

            }

            @Override
            public void onNothingSelected() {

            }
        });
    }


    private void loadBarChartData(BarChart barchart, ArrayList<IBarDataSet> dataSet) {
        Activity activity = getActivity();
        if (!isAdded() || activity == null) {
            return;
        }
        BarData data = new BarData(dataSet);

        XAxis xAxis = barchart.getXAxis();
        xAxis.setAxisMinimum(0);
        xAxis.setAxisMaximum(7);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                String[] items = getXAxisValues();
                if (value >= 0 && value < items.length) {
                    return items[(int) value];
                }
                return null;
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });

        data.setBarWidth(barWidth);
        barchart.setData(data);
        barchart.groupBars(0f, groupSpace, barSpace);
        barchart.invalidate();

    }

    private HashMap<String, float[]> getDailyData(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        HashMap<String, float[]> data = getActivityCounts(db, 0);
//        db.close();

        return data;
    }

    private ArrayList<IBarDataSet> getDataSet(HashMap<String, float[]> data, int days) {

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE, -days);


        ArrayList<BarEntry> valuesResting = new ArrayList<>();
        ArrayList<BarEntry> valueWalking = new ArrayList<>();
        ArrayList<BarEntry> valueRunning = new ArrayList<>();

        for (int i = 0; i < 7; ++i) {
            date.add(Calendar.DATE, 1);

            float[] item = data.get(format.format(date.getTime()));
            Log.i(TAG, "DATE=" + format.format(date.getTime()));
            if (item != null) {
                float sum = item[ActivityRecognizer.ACTIVITY_REST] +item[ActivityRecognizer.ACTIVITY_WALK] + item[ActivityRecognizer.ACTIVITY_RUN];
                valuesResting.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_REST]/sum));
                valueWalking.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_WALK]/sum));
                valueRunning.add(new BarEntry(i, item[ActivityRecognizer.ACTIVITY_RUN]/sum));
                Log.i(TAG, "i=" + i + " " + item[ActivityRecognizer.ACTIVITY_REST]);
            } else {
                valuesResting.add(new BarEntry(i, 0));
                valueWalking.add(new BarEntry(i, 0));
                valueRunning.add(new BarEntry(i, 0));
            }
        }

        date.add(Calendar.DATE, -2);

        BarDataSet barDataSet1 = new BarDataSet(valuesResting, "Resting");
        barDataSet1.setColor(getResources().getColor(R.color.activity_resting));
        barDataSet1.setDrawValues(false);
        BarDataSet barDataSet2 = new BarDataSet(valueWalking, "Walking");
        barDataSet2.setColor(getResources().getColor(R.color.activity_walking));
        barDataSet2.setDrawValues(false);
        BarDataSet barDataSet3 = new BarDataSet(valueRunning, "Running");
        barDataSet3.setColor(getResources().getColor(R.color.activity_running));
        barDataSet3.setDrawValues(false);


        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        dataSets.add(barDataSet3);
        return dataSets;

    }


    private String[] getXAxisValues() {
        SimpleDateFormat format = new SimpleDateFormat("E");
        Calendar date = Calendar.getInstance();
        String[] xAxis = new String[7];


        for (int i = 7 - 1; i >= 0; --i) {
            xAxis[i] = format.format(date.getTime());
            date.add(Calendar.DATE, -1);
        }

        return xAxis;
    }

    private int dpToPixels(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }


    private HashMap<String, float[]> getActivityCounts(SQLiteDatabase db, long start) {

        //select strftime('%d-%m-%Y', time,'unixepoch') as date, type, COUNT(acc._id) as count from activity LEFT JOIN accelero AS acc ON acc.aid = activity._id where time > 1481068800 group by date, activity.type;

        HashMap<String, float[]> data = new HashMap<>();

        long swstart = System.currentTimeMillis();

        String[] projection = {
                "strftime('%d-%m-%Y', " + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + ",'unixepoch') as date",
                PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                "COUNT(acc._id) as count"
        };

        String whereClause = PAWSDBHelper.ActivityEntry.COLUMN_NAME_TIME + ">? ";
        String[] whereArgs = {start + ""};

        Cursor c = db.query(
                PAWSDBHelper.ActivityEntry.TABLE_NAME + " LEFT JOIN " + PAWSDBHelper.AcceleroEntry.TABLE_NAME + " AS acc ON acc.aid = " + PAWSDBHelper.ActivityEntry.TABLE_NAME + "." + PAWSDBHelper.ActivityEntry._ID,
                projection,
                whereClause,
                whereArgs,
                "date, " + PAWSDBHelper.ActivityEntry.TABLE_NAME + "." + PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                String date = c.getString(c.getColumnIndexOrThrow("date"));
                int type = c.getInt(c.getColumnIndexOrThrow(PAWSDBHelper.ActivityEntry.COLUMN_NAME_TYPE));
                long count = c.getLong(c.getColumnIndexOrThrow("count"));

                float[] item = data.get(date);
                if (item == null) {
                    item = new float[3];
                    data.put(date, item);
                }

                item[type] = count;

                Log.i(TAG, "Got row " + date + " " + type + " " + count);
            } while (c.moveToNext());
        }

        long end = System.currentTimeMillis();
        Log.e(TAG, "Time: " + (end - swstart));

        return data;

    }



    private void openDetailView(int x, int daysOffset){


        // Find the date corresponding with the X location
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE, daysOffset + x + 1);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        long start = date.getTimeInMillis() / 1000;
        date.add(Calendar.DATE, 1);
        long end = date.getTimeInMillis() / 1000;

        // Inform MainActivity to load details page
        if (mListener != null) {
            mListener.onTimeSelected(start, end);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnPerformanceFragmentListener {
        void onTimeSelected(long start, long end);
    }

    SharedPreferences.OnSharedPreferenceChangeListener spChanged = new
            SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                                      String key) {
                    Log.i(TAG, "Got pref change for " + key);

                    if (key.equals("performance_history_days")) {
//                        loadHistoryDaysPreference(sharedPreferences);
//                        loadBarChartData();
                    }
                }
            };

}
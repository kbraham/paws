package eu.braham.apps.paws.processor;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

/**
 * Created by kbraham on 12/11/16.
 */

public class BackgroundRunner extends Thread {
    public static final String TAG = "BackgroundRunner";

    public static BackgroundRunner _instance = null;

    public Handler mHandler;

    public void run() {
        Log.i(TAG, "starting background thread");
        Looper.prepare();

        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                // process incoming messages here
                Log.i(TAG, "handleMessage");
            }
        };

        Looper.loop();
        Log.i(TAG, "started background thread");
    }

    public static void startLooper(){
        if (_instance == null) {
            _instance = new BackgroundRunner();
            _instance.start();
        }
    }

    public static boolean runTask(Runnable r) {
        if (_instance.mHandler == null) {
            return false;
        }

            _instance.mHandler.post(r);
        return true;
    }

    public static boolean runtask(Runnable r, long t){
        if (_instance.mHandler == null) {
            return false;
        }

        _instance.mHandler.postDelayed(r, t);
        return true;
    }
}
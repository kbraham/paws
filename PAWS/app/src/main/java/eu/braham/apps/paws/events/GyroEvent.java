package eu.braham.apps.paws.events;

/**
 * Created by kbraham on 10/4/16.
 *
 * examples
 *
 * G-134,45,-131
 */

public class    GyroEvent {
    private double x;
    private double y;
    private double z;

    public static GyroEvent parseText(String txt){
        GyroEvent gi = new GyroEvent();

        txt = txt.substring(1);
        String[] parts = txt.split(",");

        gi.x = Integer.valueOf(parts[0]);
        gi.y = Integer.valueOf(parts[1]);
        gi.z = Integer.valueOf(parts[2]);

        return gi;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public String toString(){
        return "GyroEvent (" + x + ", " + y + ", " + z + ")";
    }
}

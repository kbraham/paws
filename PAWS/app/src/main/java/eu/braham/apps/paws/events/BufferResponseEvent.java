package eu.braham.apps.paws.events;

import eu.braham.apps.paws.processor.CircularQueue;

/**
 * Created by coen_ on 14/11/2016.
 */

public class BufferResponseEvent {
    private CircularQueue<AcceleroEvent> queue;

    public BufferResponseEvent(CircularQueue<AcceleroEvent> queue) {
        this.queue = queue;
    }

    public CircularQueue<AcceleroEvent> getQueue() {
        return queue.clone();
    }
}

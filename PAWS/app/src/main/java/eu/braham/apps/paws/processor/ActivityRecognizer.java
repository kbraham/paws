package eu.braham.apps.paws.processor;

import android.graphics.Color;
import android.util.Log;

import eu.braham.apps.paws.events.AcceleroEvent;

/**
 * Created by kbraham on 10/10/16.
 */

public class ActivityRecognizer {


/**
 *
 * 10 sec window
 * running: 5.50756778805 0.550756778805 1.13264591044 0.58188913164
 *          10.2433312465 1.02433312465 1.83051106025 0.806177935604
 * walking: 6.19099677672 0.619099677672 0.883176897175 0.264077219503
 *          4.82296638121 0.482296638121 0.587979370212 0.105682732091
 * resting: 4.74526724375 0.474526724375 0.479894046241 0.00536732186567
 *          4.71967801986 0.471967801986 0.479822033708 0.00785423172188
 *
 * 5 sec windows
 * running: 2.53173682607 0.506347365213 0.606171956397 0.0998245911838
 *          5.96516275635 1.19303255127 1.83051106025 0.637478508981
 * walking: 2.40476606588 0.480953213177 0.557400663062 0.0764474498852
 * resting: 2.34486067396 0.468972134792 0.476736443465 0.00776430867361
 */

    public static final double THRESSHOLD_WALKING = 0.02;
    public static final double THRESSHOLD_RUNNING = 0.6;

    public static final int ACTIVITY_REST = 0;
    public static final int ACTIVITY_WALK = 1;
    public static final int ACTIVITY_RUN = 2;

    public int getCurrentActivityType(CircularQueue<AcceleroEvent> queue) {

        AcceleroEvent[] buffer = queue.getElements();

        int offset = buffer.length - 6;
        double[] sqrt = new double[5];
        double avg = 0;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < 5; ++i) {
            AcceleroEvent event = buffer[i + offset];
            if (event == null) {
                continue;
            }
            sqrt[i] = Math.sqrt(
                    (Math.pow(event.getX(), 2) + Math.pow(event.getY(), 2) + Math.pow(event.getZ(), 2))
            );
            avg += sqrt[i];
            if (sqrt[i] > max) {
                max = sqrt[i];
            }
        }

        avg /= 5;
        avg = max - avg;
        int act = ACTIVITY_REST;


        if (avg > ActivityRecognizer.THRESSHOLD_RUNNING) {
            act = ActivityRecognizer.ACTIVITY_RUN;
        } else if (avg > ActivityRecognizer.THRESSHOLD_WALKING) {
            act = ActivityRecognizer.ACTIVITY_WALK;
        }

        Log.i("ActRec", "Thresshold: " + avg + " act " + act);

        return act;
    }
}

package eu.braham.apps.paws.fragments;

/**
 * Created by kbraham on 11/30/16.
 */

/* Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCircleClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import eu.braham.apps.paws.R;


/**
 * This shows how to draw circles on a map.
 */
public class CircleDemoActivity extends AppCompatActivity implements OnSeekBarChangeListener,
        OnMarkerDragListener, OnMapLongClickListener, OnMapReadyCallback {

    public static final double RADIUS_OF_EARTH_METERS = 6371009;

    public static final double GEOFENCE_LAT_DEFAULT = 52.238998;
    public static final double GEOFENCE_LON_DEFAULT = 6.856575;

    public static final int GEOFENCE_AREA_MAX = 500;
    public static final int GEOFENCE_AREA_DEFAULT = 50;
    public static final int GEOFEANCE_AREA_STROKE = 10;

    private GoogleMap mMap;

    private List<DraggableCircle> mCircles = new ArrayList<DraggableCircle>(1);

    private SeekBar mAreaBar;
    private int mStrokeColor;
    private int mFillColor;
//    private CheckBox mClickabilityCheckbox;
    private TextView mGeofenceDesc;

    private class DraggableCircle {
        private final int id;
        private final Marker centerMarker;
        private final Marker radiusMarker;
        private final Circle circle;
        private double radius;
        public DraggableCircle(LatLng center, double radius, boolean clickable) {
            this.radius = radius;
            centerMarker = mMap.addMarker(new MarkerOptions()
                    .position(center)
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.defaultMarker(
                            BitmapDescriptorFactory.HUE_AZURE)));
            radiusMarker = mMap.addMarker(new MarkerOptions()
                    .position(toRadiusLatLng(center, radius))
                    .draggable(true));
            circle = mMap.addCircle(new CircleOptions()
                    .center(center)
                    .radius(radius)
                    .strokeWidth(GEOFEANCE_AREA_STROKE)
                    .strokeColor(mStrokeColor)
                    .fillColor(mFillColor)
                    .clickable(clickable));
            id = 1;
            updateGeofenceDesc(radius);
        }

        public DraggableCircle(LatLng center, LatLng radiusLatLng, boolean clickable) {
            this.radius = toRadiusMeters(center, radiusLatLng);
            centerMarker = mMap.addMarker(new MarkerOptions()
                    .position(center)
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.defaultMarker(
                            BitmapDescriptorFactory.HUE_AZURE)));
            radiusMarker = mMap.addMarker(new MarkerOptions()
                    .position(radiusLatLng)
                    .draggable(true));
            circle = mMap.addCircle(new CircleOptions()
                    .center(center)
                    .radius(radius)
                    .strokeWidth(GEOFEANCE_AREA_STROKE)
                    .strokeColor(mStrokeColor)
                    .fillColor(mFillColor)
                    .clickable(clickable));

            id = 1;
            updateGeofenceDesc(radius);
        }

        public boolean onMarkerMoved(Marker marker) {
            if (marker.equals(centerMarker)) {
                circle.setCenter(marker.getPosition());
                radiusMarker.setPosition(toRadiusLatLng(marker.getPosition(), radius));
                storeGeofencePoint(id, this.centerMarker.getPosition(), radius);
                return true;
            }
            if (marker.equals(radiusMarker)) {
                radius = toRadiusMeters(centerMarker.getPosition(), radiusMarker.getPosition());
                updateGeofenceDesc(radius);
                circle.setRadius(radius);
                storeGeofencePoint(id, this.centerMarker.getPosition(), radius);
                return true;
            }
            return false;
        }

//        public void onStyleChange() {
//            circle.setStrokeWidth(mWidthBar.getProgress());
//            circle.setFillColor(mFillColor);
//            circle.setStrokeColor(mStrokeColor);
//        }


        public void setRadius(double radius){
            this.radius = radius;
            radiusMarker.setPosition(toRadiusLatLng(centerMarker.getPosition(), radius));
            circle.setRadius(radius);
            updateGeofenceDesc(radius);
            storeGeofencePoint(id, this.centerMarker.getPosition(), radius);
        }
        public void setClickable(boolean clickable) {
            circle.setClickable(clickable);
        }
    }

    /** Generate LatLng of radius marker */
    private static LatLng toRadiusLatLng(LatLng center, double radius) {
        double radiusAngle = Math.toDegrees(radius / RADIUS_OF_EARTH_METERS) /
                Math.cos(Math.toRadians(center.latitude));
        return new LatLng(center.latitude, center.longitude + radiusAngle);
    }

    private static double toRadiusMeters(LatLng center, LatLng radius) {
        float[] result = new float[1];
        Location.distanceBetween(center.latitude, center.longitude,
                radius.latitude, radius.longitude, result);
        return result[0];
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.circle_demo);

        mAreaBar = (SeekBar) findViewById(R.id.geofenceSeekBar);
        mAreaBar.setMax(GEOFENCE_AREA_MAX);
        mAreaBar.setProgress(GEOFENCE_AREA_DEFAULT);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        mClickabilityCheckbox = (CheckBox) findViewById(R.id.toggleClickability);
        mGeofenceDesc = (TextView) findViewById(R.id.geofenceDesc);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        // Override the default content description on the view, for accessibility mode.
        map.setContentDescription("Description");

        mAreaBar.setOnSeekBarChangeListener(this);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);

        mFillColor = Color.HSVToColor(
                20, new float[]{20, 1, 1});
        mStrokeColor = Color.BLACK;


        // Get geofence point
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        double latitude = Double.longBitsToDouble(sharedPref.getLong("geofence-lat", Double.doubleToLongBits(GEOFENCE_LAT_DEFAULT)));
        double longitude = Double.longBitsToDouble(sharedPref.getLong("geofence-lon", Double.doubleToLongBits(GEOFENCE_LON_DEFAULT)));
        double radius =  Double.longBitsToDouble(sharedPref.getLong("geofence-rad", Double.doubleToLongBits(GEOFENCE_AREA_DEFAULT)));
        LatLng point = new LatLng(latitude, longitude);
        DraggableCircle circle =
                new DraggableCircle(point, radius, true);
        mCircles.add(circle);

        // Move the map so that it is centered on the initial circle
        CameraPosition cameraPosition = new CameraPosition.Builder().target(point).zoom(16).build();
//                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SYDNEY, 4.0f));

        // Set up the click listener for the circle.
        map.setOnCircleClickListener(new OnCircleClickListener() {
            @Override
            public void onCircleClick(Circle circle) {
                // Flip the r, g and b components of the circle's stroke color.
                int strokeColor = circle.getStrokeColor() ^ 0x00ffffff;
                circle.setStrokeColor(strokeColor);
            }
        });
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // Don't do anything here.
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // Don't do anything here.
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar == mAreaBar) {
            for (DraggableCircle draggableCircle : mCircles) {
                draggableCircle.setRadius(progress);
            }
        }

    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        onMarkerMoved(marker);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        onMarkerMoved(marker);
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        onMarkerMoved(marker);
    }

    private void onMarkerMoved(Marker marker) {
        for (DraggableCircle draggableCircle : mCircles) {
            if (draggableCircle.onMarkerMoved(marker)) {
                break;
            }
        }
    }

    @Override
    public void onMapLongClick(LatLng point) {
//        // We know the center, let's place the outline at a point 3/4 along the view.
//        View view = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
//                .getView();
//        LatLng radiusLatLng = mMap.getProjection().fromScreenLocation(new Point(
//                view.getHeight() * 3 / 4, view.getWidth() * 3 / 4));
//
//        // ok create it
//        DraggableCircle circle =
//                new DraggableCircle(point, radiusLatLng, mClickabilityCheckbox.isChecked());
//        mCircles.add(circle);
    }

    public void toggleClickability(View view) {
        boolean clickable = ((CheckBox) view).isChecked();
        // Set each of the circles to be clickable or not, based on the
        // state of the checkbox.
        for (DraggableCircle draggableCircle : mCircles) {
            draggableCircle.setClickable(clickable);
        }
    }

    public void updateGeofenceDesc(double radius){
        mGeofenceDesc.setText("Geofence area " + radius + "m");
    }

    public void storeGeofencePoint(int id, LatLng pos, double radius){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("geofence-lat", Double.doubleToLongBits(pos.latitude));
        editor.putLong("geofence-lon", Double.doubleToLongBits(pos.longitude));
        editor.putLong("geofence-rad", Double.doubleToLongBits(radius));
        editor.commit();
    }
}
package eu.braham.apps.paws;


import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.intesens.kinto_http.ClientException;
import com.intesens.kinto_http.KintoClient;
import com.intesens.kinto_http.KintoException;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import eu.braham.apps.paws.events.BLEEvent;
import eu.braham.apps.paws.events.BufferRequestEvent;
import eu.braham.apps.paws.events.BufferResponseEvent;
import eu.braham.apps.paws.events.GPSEvent;
import eu.braham.apps.paws.events.SendBLEMessage;
import eu.braham.apps.paws.fragments.CalendarMonthFragment;
import eu.braham.apps.paws.fragments.CircleDemoActivity;
import eu.braham.apps.paws.fragments.DebugFragment;
import eu.braham.apps.paws.fragments.MapFragment;
import eu.braham.apps.paws.fragments.PerformanceFragment;
import eu.braham.apps.paws.fragments.ScanFragment;
import eu.braham.apps.paws.fragments.SensorsFragment;
import eu.braham.apps.paws.fragments.SyncFragment;
import eu.braham.apps.paws.processor.AGPSProcessor;
import eu.braham.apps.paws.processor.BackgroundRunner;
import eu.braham.apps.paws.services.SensorsService;
import eu.braham.apps.paws.services.SyncService;
import eu.braham.apps.paws.services.TagService;
import eu.braham.apps.paws.services.UbloxOfflineService;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ScanFragment.OnScanFragmentListener, PerformanceFragment.OnPerformanceFragmentListener {

    private static final String TAG = "MainActivity";

    private static final boolean ENABLE_GPS = true;

    private static final int SYNC_WAIT_TIME = 10000;

    private TagService mService = null;
    private SensorsService mServiceSensor = null;
    private boolean mConnected = false;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mDevice = null;
    private AGPSProcessor mAGPS = new AGPSProcessor();

    public static final int REQUEST_ENABLE_BT = 1;
    public static final int REQUEST_SETTINGS = 2;

    public static final String EXTRAS_DEVICE_NAME = "DeviceName";
    public static final String EXTRAS_DEVICE_ADDRESS = "DeviceAddress";


    private Intent mServiceIntent;
    private Intent mSyncServiceIntent;

    private int mAccFrequency = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        BackgroundRunner.startLooper();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        boolean bluetoothCapable = true;

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            bluetoothCapable = false;
        }

        if (bluetoothCapable) {
            // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
            // BluetoothAdapter through BluetoothManager.
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = bluetoothManager.getAdapter();

            // Checks if Bluetooth is supported on the device.
            if (mBluetoothAdapter == null) {
                Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
                finish();
                return;
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Log.i(TAG, "onClick - BT not enabled yet");
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            }

            serviceInit();
        }

        if (!checkLocationPermission()) {
            Log.d(TAG, "No location permission!");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            Log.d(TAG, "Got location permission!");
        }


        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                onTabSwitch(tabId);
            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                onTabSwitch(tabId);
            }
        });

        // Fetch UBLOX
        mServiceIntent = new Intent(this, UbloxOfflineService.class);
        startService(mServiceIntent);
        LocalBroadcastManager.getInstance(this).registerReceiver(agpsDownloadReceiver, makeUBloxIntentFilter());

        // Start syncing
        mSyncServiceIntent = new Intent(this, SyncService.class);
        startService(mSyncServiceIntent);
        LocalBroadcastManager.getInstance(this).registerReceiver(syncReceiver, makeSyncIntentFilter());

        // Start eventbus
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mService != null) {
            mService.disconnect();
            mService.close();
            LocalBroadcastManager.getInstance(this).unregisterReceiver(TagStatusChangeReceiver);
            unbindService(mServiceConnection);
            unbindService(mServiceConnectionSensor);
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(agpsDownloadReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(syncReceiver);

        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private static IntentFilter makeUBloxIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UbloxOfflineService.NOTIFICATION);
        return intentFilter;
    }

    private static IntentFilter makeSyncIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SyncService.SYNC_COMPLETE);
        return intentFilter;
    }

    private void serviceInit() {
        Intent bindIntent = new Intent(this, TagService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(this).registerReceiver(TagStatusChangeReceiver, makeGattUpdateIntentFilter());

        Intent bind2Intent = new Intent(this, SensorsService.class);
        bindService(bind2Intent, mServiceConnectionSensor, Context.BIND_AUTO_CREATE);

    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TagService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(TagService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(TagService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(TagService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(TagService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((TagService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + mService);
            if (!mService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
            String deviceAddress = sharedPref.getString(getString(R.string.pref_device_address), null);

            if (deviceAddress != null) {
                mService.connect(deviceAddress);
            }

            String accFreq = sharedPref.getString("acc_frequency", "1");
            try {
                mAccFrequency = Integer.parseInt(accFreq);
            } catch (NumberFormatException e){
            }

        }

        public void onServiceDisconnected(ComponentName classname) {
            ////     mService.disconnect(mDevice);
            mService = null;
        }
    };

    private ServiceConnection mServiceConnectionSensor = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mServiceSensor = ((SensorsService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnectedSensor mService= " + mServiceSensor);
        }

        public void onServiceDisconnected(ComponentName classname) {
            mServiceSensor = null;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            boolean done = getSupportFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    private void loadFragment(CharSequence title, Fragment fragment, boolean addToStack) {
        setTitle(title);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if(addToStack){
            ft.addToBackStack(null);
        }
        ft.replace(R.id.flContent, fragment);
        ft.commit();

        invalidateOptionsMenu();
    }

    private void onTabSwitch(@IdRes int tabId) {

        Class fragmentClass = null;
        String title = "";

        if (tabId == R.id.tab_performance) {
            fragmentClass = PerformanceFragment.class;
            title = "Performance";
        } else if (tabId == R.id.tab_monthchart) {
            fragmentClass = CalendarMonthFragment.class;
            title = "Sensors";

        } else if (tabId == R.id.tab_sensors) {
            fragmentClass = SensorsFragment.class;
            title = "Sensors";
        } else if (tabId == R.id.tab_map) {
            fragmentClass = MapFragment.class;
            title = "Map";
        } else if (tabId == R.id.tab_debug) {
            fragmentClass = DebugFragment.class;
            title = "Debug";
        }

        try {
            Fragment fragment = (Fragment) fragmentClass.newInstance();
            loadFragment(title, fragment, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Class fragmentClass = null;
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_performance) {
        } else if (id == R.id.nav_map) {

// TODO: Make GEOFENCE option in the menus
            Intent i = new Intent(this, CircleDemoActivity.class);
            startActivity(i);

            return true;
        } else if (id == R.id.nav_devices) {
            fragmentClass = ScanFragment.class;
        } else if (id == R.id.nav_sync) {
            fragmentClass = SyncFragment.class;
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent, REQUEST_SETTINGS);
            return true;
        }

        try {
            Fragment fragment = (Fragment) fragmentClass.newInstance();
            loadFragment(item.getTitle(), fragment, false);
        } catch (Exception e) {
            e.printStackTrace();
        }


        item.setChecked(true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDeviceSelected(Bundle b) {
        String deviceAddress = b.getString(EXTRAS_DEVICE_ADDRESS);
        String deviceName = b.getString(EXTRAS_DEVICE_NAME);
        mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);

        Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
        showMessage(mDevice.getName());
        mService.connect(deviceAddress);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.pref_device_address), deviceAddress);
        editor.putString(getString(R.string.pref_device_name), deviceName);
        editor.commit();

    }

    public boolean checkLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private BroadcastReceiver agpsDownloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, "Got UBLOX INTENT! ");


            try {
                mAGPS.parseAGPS(new File(getFilesDir(), UbloxOfflineService.FILE_NAME));
                Log.i(TAG, "loading finish");

            } catch (Exception ex) {
                Log.i(TAG, ex.toString());
            }
        }
    };


    private BroadcastReceiver syncReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, "Got sync INTENT! ");

            BackgroundRunner.runtask(syncTask, SYNC_WAIT_TIME);
        }
    };

    private final BroadcastReceiver TagStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            final Intent mIntent = intent;
            //*********************//
            if (action.equals(TagService.ACTION_GATT_CONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        mConnected = true;
                        showMessage("connected");

                    }
                });
            }

            //*********************//
            if (action.equals(TagService.ACTION_GATT_DISCONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        showMessage("disconnected");
                        mService.close();
                        mConnected = false;
                    }
                });
            }


            //*********************//
            if (action.equals(TagService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mService.enableTXNotification();
            }

            //*********************//
            if (action.equals(TagService.DEVICE_CONNECTION_FAILED)) {
                Log.e(TAG, "TAGSERVICE TX connection failed");
                mService.enableTXNotification();
            }

            if (action.equals(TagService.ACTION_DATA_AVAILABLE)) {

                final byte[] txValue = intent.getByteArrayExtra(TagService.EXTRA_DATA);
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            String text = new String(txValue, "UTF-8");

                            if (text.equals("agpsp")) {
                                if (!mAGPS.isReady()) {
                                    Log.e(TAG, "No AGPS info available!");
                                    EventBus.getDefault().post(new BLEEvent("! No AGPS info available"));
                                } else {
                                    Log.d(TAG, "Sending update info! B" + mAGPS.getCurrent().length);
                                    Log.d(TAG, "Bytes: " + bytesToHex(mAGPS.getCurrent()));

                                    if (ENABLE_GPS) {
                                        sendBuf2Tag(mAGPS.getCurrent());
                                        EventBus.getDefault().post(new GPSEvent(GPSEvent.STATUS_UPLOAD, mAGPS.getIndex(), mAGPS.getSize()));
                                    } else {
                                        sendCmd2Tag("agpsdone");
                                        sendAccFrequency();
                                    }
                                }
                            } else if (text.equals("agpsn")) {
                                byte[] agpsBytes = mAGPS.getNext();
                                if (agpsBytes == null) {
                                    sendCmd2Tag("agpsdone");
                                    sendAccFrequency();
                                    EventBus.getDefault().post(new GPSEvent(GPSEvent.STATUS_UPLOAD, mAGPS.getSize(), mAGPS.getSize()));
                                } else {
                                    EventBus.getDefault().post(new GPSEvent(GPSEvent.STATUS_UPLOAD, mAGPS.getIndex(), mAGPS.getSize()));
                                    sendBuf2Tag(agpsBytes);
                                }
                            }

                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                });
            }
            //*********************//
            if (action.equals(TagService.DEVICE_DOES_NOT_SUPPORT_UART)) {
                showMessage("Device doesn't support UART. Disconnecting");
                mService.disconnect();
            }

        }
    };


    private void sendAccFrequency(){
        sendCmd2Tag("setacc " + mAccFrequency);
    }

    /****
     * BLUETOOTH COMMS
     ****/

    private void sendBuf2Tag(byte[] s) {
//        if (mConnected == false) {
//            showMessage("not connected");
//            mBleSwitch.setChecked(true);
//            return;
//        }
        byte[] value;

        try {
            int len = s.length;
            int offset = 0;
            String tmp;
            while (len > 20) {

                value = Arrays.copyOfRange(s, offset, offset + 20);
                Log.d(TAG, "sendBuf loop " + len + " " + offset);
                mService.writeRXCharacteristic(value);
                offset += 20;
                len -= 20;
                //Log.d(TAG, tmp);

            }

            if (len > 0) {
                value = Arrays.copyOfRange(s, offset, offset + len);
                //Log.d(TAG, "sending..." + tmp);
                Log.d(TAG, "sendBuf loop " + len + " " + offset);
                mService.writeRXCharacteristic(value);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendCmd2Tag(String s) {
//        if (mConnected == false) {
//            showMessage("not connected");
//            mBleSwitch.setChecked(true);
//            return;
//        }
        byte[] value;
        final String sendStr = s + '\r';

        try {
//            if (mConsoleFragment != null) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
//                        mConsoleFragment.updateView("[" + currentDateTimeString + "] TX: " + ConvertStringToHex(tagCmd));
//                        Log.i(TAG, ConvertStringToHex(sendStr));
//                    }
//                });
//
//            }
            int len = sendStr.length();
            int offset = 0;
            String tmp;
            while (len > 20) {
                tmp = sendStr.substring(offset, offset + 20);
                value = tmp.getBytes("UTF-8");
                mService.writeRXCharacteristic(value);
                offset += 20;
                len -= 20;
            }

            if (len > 0) {
                tmp = sendStr.substring(offset, offset + len);
                value = tmp.getBytes("UTF-8");
                //Log.d(TAG, "sending..." + tmp);
                mService.writeRXCharacteristic(value);
            }

            String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
            EventBus.getDefault().post(new BLEEvent("< " + currentDateTimeString + " reboot"));

        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    @Override
    public void onTimeSelected(long start, long end) {
        Log.e("My Chart", "My Chart " + start + " - " + end);

        MapFragment fragment = MapFragment.newInstance(start, end);
        loadFragment("Details", fragment, true);
    }

    public double[] getGeofenceSettings(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        double latitude = Double.longBitsToDouble(sharedPref.getLong("geofence-lat", Double.doubleToLongBits(CircleDemoActivity.GEOFENCE_LAT_DEFAULT)));
        double longitude = Double.longBitsToDouble(sharedPref.getLong("geofence-lon", Double.doubleToLongBits(CircleDemoActivity.GEOFENCE_LON_DEFAULT)));
        double radius =  Double.longBitsToDouble(sharedPref.getLong("geofence-rad", Double.doubleToLongBits(CircleDemoActivity.GEOFENCE_AREA_DEFAULT)));

        return new double[]{latitude,longitude,radius};
    }

    Runnable syncTask = new Runnable() {
        @Override
        public void run() {
            mSyncServiceIntent = new Intent(MainActivity.this, SyncService.class);
            startService(mSyncServiceIntent);
        }
    };

    SharedPreferences.OnSharedPreferenceChangeListener spChanged = new
            SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                                      String key) {
                    Log.i(TAG, "Got pref change for " + key);

                    if (key.equals("acc_frequency")) {

                        String accFreq = sharedPreferences.getString("acc_frequency", "1");
                        try {
                            mAccFrequency = Integer.parseInt(accFreq);
                        } catch (NumberFormatException e){
                        }

                        sendAccFrequency();
                    }
                }
            };

    @Subscribe
    public void onMessage(SendBLEMessage event) {
        Log.i(TAG, "Sending reboot cmd");
        sendCmd2Tag(event.getMessage());
    }
}

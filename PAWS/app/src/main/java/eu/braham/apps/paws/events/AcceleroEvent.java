package eu.braham.apps.paws.events;

/**
 * Created by kbraham on 10/4/16.
 *
 * examples
 * A-644,288,-16052
 */

public class AcceleroEvent {
    private double x;
    private double y;
    private double z;

    private static final double SCALE_DIVISOR = 16384;

    public static AcceleroEvent parseText(String txt){
        AcceleroEvent ai = new AcceleroEvent();

        txt = txt.substring(1);
        String[] parts = txt.split(",");

        ai.x = Double.valueOf(parts[0])/SCALE_DIVISOR;
        ai.y = Double.valueOf(parts[1])/SCALE_DIVISOR;
        ai.z = Double.valueOf(parts[2])/SCALE_DIVISOR;

        return ai;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public String toString(){
        return "AcceleroEvent (" + x + ", " + y + ", " + z + ")";
    }
}

import sqlite3
conn = sqlite3.connect('paws.db')


c = conn.cursor()

# Create table

# Activities table (id, type, beginTime, duration)

c.execute('''CREATE TABLE activities (id INTEGER PRIMARY KEY, type INTEGER, beginTime TEXT, duration INTEGER)''')



#c.execute('''CREATE TABLE stocks
#             (date text, trans text, symbol text, qty real, price real)''')

# Insert a row of data
#c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")

c.execute("INSERT INTO activities VALUES (null, 1, 'a', 101)")


# Accelero info

c.execute('''CREATE TABLE accelero (id INTEGER PRIMARY KEY, aid INTEGER, x INTEGER, y INTEGER, z INTEGER, timestamp INTEGER)''')

# Generate some noise

import numpy as np

noise1 = np.random.normal(0,1,100)
noise2 = np.random.normal(0,1,100)
noise3 = np.random.normal(0,1,100)

# 0 is the mean of the normal distribution you are choosing from
# 1 is the standard deviation of the normal distribution
# 100 is the number of elements you get in array noise

for i in range(100):
    c.execute("INSERT INTO accelero VALUES (null, 1, %d, %d, %d, %d)" % (noise1[i],noise2[i],noise3[i], i))



# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
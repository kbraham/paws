import sqlite3
import numpy as np
import matplotlib.pyplot as plt
import json
import math

with open('logs/post_2016-11-08_16-50-19.log') as data_file:    
    data = json.load(data_file)

x = []
y = []
z = []

t = (1,)
for row in data["accel"]:
  x.append(row["x"])
  y.append(row["y"])
  z.append(row["z"])
#print(c.fetchone())

#x = x[:100]
#y = y[:100]
#z = z[:100]

xs = np.array(x)
ys = np.array(y)
zs = np.array(z)

plt.plot(xs, 'r', ys, 'b', zs, 'g')

#np.fromiter(c.fetchall(), np.float)
#line, = plt.plot(x, np.sin(x), '--', linewidth=2)
#plt.show()
plt.figure(2)


bs = xs.copy()
for i in range(len(xs)):
  bs[i] = math.sqrt(math.pow(xs[i], 2)+ math.pow(ys[i], 2)+ math.pow(zs[i], 2))


plt.plot(bs, 'b')

#np.fromiter(c.fetchall(), np.float)
#line, = plt.plot(x, np.sin(x), '--', linewidth=2)
#plt.show()
plt.figure(3)


import scipy
import scipy.fftpack
import pylab
from scipy import pi
t = scipy.linspace(0,len(x), len(x))
acc = lambda t: 10*scipy.sin(2*pi*2.0*t) + 5*scipy.sin(2*pi*8.0*t) + 2*scipy.random.random(len(t))

signal = acc(t)

FFT = abs(scipy.fft(bs))
freqs = scipy.fftpack.fftfreq(xs.size, t[1]-t[0])

print(t.size, xs.size)

pylab.subplot(211)
pylab.plot(t, bs)
pylab.subplot(212)
pylab.plot(freqs,20*scipy.log10(FFT),'x')
pylab.show()


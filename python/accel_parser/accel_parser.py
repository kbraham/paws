import sqlite3
import numpy as np
import matplotlib.pyplot as plt
import math

conn = sqlite3.connect('paws.db')


c = conn.cursor()

x = []
y = []
z = []

t = (10,)
t = (9,)

# t = (15,)
# t = (18,)
t = (2,)
for row in c.execute('SELECT * FROM accelero WHERE aid=?', t):
  x.append(row[2])
  y.append(row[3])
  z.append(row[4])
#print(c.fetchone())


# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()

start = 0
stop = 10000
stop = 500

# Jip walking
# start = 65
# stop = 80

#start = 80
#stop = 120

# 10 sec window
# running: 5.50756778805 0.550756778805 1.13264591044 0.58188913164
#          10.2433312465 1.02433312465 1.83051106025 0.806177935604
# walking: 6.19099677672 0.619099677672 0.883176897175 0.264077219503
#          4.82296638121 0.482296638121 0.587979370212 0.105682732091
# resting: 4.74526724375 0.474526724375 0.479894046241 0.00536732186567
#          4.71967801986 0.471967801986 0.479822033708 0.00785423172188

# 5 sec windows
# running: 2.53173682607 0.506347365213 0.606171956397 0.0998245911838
#          5.96516275635 1.19303255127 1.83051106025 0.637478508981
# walking: 2.40476606588 0.480953213177 0.557400663062 0.0764474498852
# resting: 2.34486067396 0.468972134792 0.476736443465 0.00776430867361


# running: 0.637478508981
# walking: 0.0764474498852
# resting: 0.00776430867361

x = x[start:stop]
y = y[start:stop]
z = z[start:stop]

xs = np.array(x)
ys = np.array(y)
zs = np.array(z)

plt.plot(xs, 'r', ys, 'b', zs, 'g')

#np.fromiter(c.fetchall(), np.float)
#line, = plt.plot(x, np.sin(x), '--', linewidth=2)
#plt.show()
plt.figure(2)


bs = xs.copy()
for i in range(len(xs)):
  bs[i] = math.sqrt(math.pow(xs[i], 2)+ math.pow(ys[i], 2)+ math.pow(zs[i], 2))


sum = 0
offset = 0#30
lengthy = 5
maxVal = -100

for i in range(lengthy):
  sum += bs[offset+i]
  if bs[offset+i] > maxVal:
    maxVal = bs[offset+i] 

print (sum, sum/lengthy, maxVal, maxVal-(sum/lengthy))


sum = 0
lengthy = len(xs)
maxVal = -100
for i in range(lengthy):
  sum += bs[i]
  if bs[i] > maxVal:
    maxVal = bs[i] 


print (sum, sum/lengthy, maxVal, maxVal-(sum/lengthy))

avg_line = xs.copy()
max_line = xs.copy()




from collections import deque
from statistics import mean
window = deque()

# Load first 5 elements
for i in range(5):
  window.append(bs[i])

# loop through all items
for i in range(5, lengthy):
  avg_line[i] = mean(window)
  max_line[i] = max(window)
  window.popleft()
  window.append(bs[i])

plt.plot(bs, 'b', avg_line, 'r', max_line, 'g--')

#np.fromiter(c.fetchall(), np.float)
#line, = plt.plot(x, np.sin(x), '--', linewidth=2)
#plt.show()
plt.figure(3)


import scipy
import scipy.fftpack
import pylab
from scipy import pi
t = scipy.linspace(0,len(x), len(x))
acc = lambda t: 10*scipy.sin(2*pi*2.0*t) + 5*scipy.sin(2*pi*8.0*t) + 2*scipy.random.random(len(t))

signal = acc(t)

FFT = abs(scipy.fft(bs))
freqs = scipy.fftpack.fftfreq(xs.size, t[1]-t[0])

print(t.size, xs.size)

pylab.subplot(211)
pylab.plot(t, bs)
pylab.subplot(212)
pylab.plot(freqs,20*scipy.log10(FFT),'x')
pylab.show()

